-- Aqui toda la declaracion guia de la base de datos bro

-- Crear la tabla de roles
CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    role_name VARCHAR(50) NOT NULL UNIQUE
);

-- Insertar roles iniciales
INSERT INTO roles (role_name) VALUES ('admin'), ('general');

-- Crear la tabla de usuarios
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    profile_image VARCHAR(255),
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Crear la tabla de usuario-rol
CREATE TABLE user_roles (
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (role_id) REFERENCES roles(id)
);

-- Crear la tabla de proyectos con la columna de array
CREATE TABLE projects (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    image VARCHAR(255),
    logo VARCHAR(255),
    author_id INTEGER NOT NULL,
    collaboration VARCHAR(255),
    tags TEXT[],  -- Array de tags
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    resume VARCHAR,
    FOREIGN KEY (author_id) REFERENCES users(id)
);


-- Insertar datos de ejemplo en la tabla de usuarios
INSERT INTO users (id, full_name, profile_image, email, password, created_at, updated_at)
VALUES
(1, 'Jeisson León', '/img/pic_profile.png', 'jeisson@example.com', 'password', '2024-09-27 18:21:32.566707', '2024-09-27 18:21:32.566707'),
(2, 'Karen Hernandez', '/img/pic_profile_2.jpeg', 'karen@email.com', 'passwa', '2024-09-27 15:18:18.991606', '2024-09-27 15:18:18.991606'),
(3, 'Samuel Pirobo', '/img/pic_profile_2.jpeg', 'samuel@email.com', 'passwa', '2024-09-27 15:18:18.991606', '2024-09-27 15:18:18.991606');

-- Insertar datos de ejemplo en la tabla de usuario-rol
INSERT INTO user_roles (user_id, role_id)
VALUES (1, 1);

-- Insertacion de datos de ejemplo en la tabla de proyectos
INSERT INTO projects (id, title, description, image, logo, author_id, collaboration, tags, created_at, updated_at, resume)
VALUES
(5, 'Eventium Project (Events)', 'Event management application with real-time registration and resource optimization.', '/img/background_example.png', '/img/logo_example.png', 1, NULL, ARRAY['OPEN_SOURCE', 'WEB_DEV', 'UX_UI', 'INNOVATION', 'APIS', 'JAVASCRIPT', 'NODEJS', 'EXPRESS', 'REACT', 'HTML', 'CSS', 'GIT', 'DOCKER', 'MONGODB', 'FIGMA', 'BASH', 'MOCKUPS', 'TESTING_QA', 'DESIGN', 'ALGORITHMS'], '2024-09-30 16:32:38.929481', '2024-09-30 16:32:38.929481', NULL),
(6, 'Saveurs Françaises', 'E-commerce platform for French recipes with authentication and gourmet product purchase.', '/img/background_example.png', '/img/logo_example.png', 1, NULL, ARRAY['WEB_DEV', 'UX_UI', 'CLOUD', 'HTML', 'CSS', 'JAVASCRIPT', 'REACT', 'NODEJS', 'MARIADB'], '2024-09-30 19:35:20.161434', '2024-09-30 19:35:20.161434', NULL),
(7, 'Card Task', 'Task management application with a simple interface and cross-platform access.', '/projects-source/public_chat_demo.png', '/projects-source/logo_public_chat.png', 1, NULL, ARRAY['WEB_DEV', 'UX_UI', 'DEVOPS', 'REACT', 'HTML', 'CSS', 'JAVASCRIPT', 'GIT', 'APIS', 'ARCHITECTURE', 'NODEJS'], '2024-10-01 01:35:29.911009', '2024-10-01 01:35:29.911009', NULL),
(2, 'Autocommit AI (Beta Alpha)', 'Un script automatizado que gestiona el proceso de commits en Git, ejecutándose cada 5 minutos para detectar cambios y generar mensajes de commit significativos con ayuda de inteligencia artificial.', '/projects-source/autocommit_demo.png', '/projects-source/logo_autocommit.jpeg', 1, 'Lion Labs', ARRAY['PYTHON', 'AUTOMATION', 'AI_ML', 'SOFTWARE', 'EXPERIMENTS', 'LINUX'], '2024-09-27 18:25:08.462145', '2024-09-27 18:25:08.462145', NULL),
(3, 'FlowStock (Inventory Management)', 'Aplicación para administrar inventarios de manera fácil, eficiente y segura. La filosofía del proyecto es hacer que la administración de inventarios sea lo más accesible posible y proporcionar la mejor experiencia para el usuario. Ofrece autenticación sin contraseña, cifrado completo de datos, y generación de informes y gráficos.', '/projects-source/flowstock_demo.png', '/projects-source/logo_flowstock.png', 1, 'Lion Team', ARRAY['OPEN_SOURCE', 'WEB_DEV', 'UX_UI', 'INNOVATION', 'CLOUD', 'SVELTEKIT', 'SVELTE', 'ARCHITECTURE', 'JAVASCRIPT', 'NODEJS', 'APIS'], '2024-09-30 15:56:30.973489', '2024-09-30 15:56:30.973489', NULL),
(1, 'Portfolio Jeisson León', 'Este portafolio web es una presentación visual y técnica de mi experiencia en diferentes areas. Ha sido diseñado con un enfoque en la simplicidad y la efectividad, permitiendo a los reclutadores y clientes potenciales conocer mis habilidades, proyectos y trayectoria de manera ágil y organizada.', '/projects-source/portafolio_demo.png', '/projects-source/logo_portafolio.png', 1, NULL, ARRAY['WEB_DEV', 'UX_UI', 'INNOVATION', 'DEVOPS', 'JAVASCRIPT', 'NODEJS', 'REACT', 'NEXTJS', 'HTML', 'CSS', 'GIT', 'DOCKER', 'POSTGRESQL', 'FIGMA', 'TESTING_QA', 'DESIGN', 'OPEN_SOURCE', 'MOCKUPS'], '2024-09-27 18:25:08.462145', '2024-09-27 18:25:08.462145', NULL),
(4, 'Public Chat Project', 'Plataforma de chat en tiempo real desarrollada con React y una base de datos NoSQL para gestionar mensajes de forma eficiente. Permite a los usuarios registrarse solo con un nombre de usuario y comunicarse instantáneamente. Utiliza una API para la transmisión de datos y la eliminación automática de mensajes.', '/projects-source/public_chat_demo.png', '/projects-source/logo_public_chat.png', 1, NULL, ARRAY['OPEN_SOURCE', 'WEB_DEV', 'UX_UI', 'INNOVATION', 'APIS', 'JAVASCRIPT', 'NODEJS', 'EXPRESS', 'REACT', 'HTML', 'CSS', 'GIT', 'DOCKER', 'MONGODB', 'FIGMA', 'BASH', 'MOCKUPS', 'TESTING_QA', 'DESIGN', 'ALGORITHMS'], '2024-09-30 16:32:38.929481', '2024-09-30 16:32:38.929481', NULL);
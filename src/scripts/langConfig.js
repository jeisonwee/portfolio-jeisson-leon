// Sistema de control para el uso de lenguajes disponibles
import { cookies } from "next/headers";

// Traducciones disponibles
import es from "@/dictionaries/es.json";
import en from "@/dictionaries/en.json";

// Objeto que mapea códigos de idioma a los diccionarios correspondientes
const languages = {
  en,
  es,
};

const verifyLang = () => {
  let cookieStore = cookies();
  let preferredLocale = cookieStore.get("preferredLocale") ? cookieStore.get("preferredLocale") : { value: "en" };

  return preferredLocale;
}

// Esta funcion devuelve el tipo de idioma preferido
export const langUserPre = () => {
  const prefLang = verifyLang();

  if (!prefLang) {
    return "en";
  }

  return prefLang.value;
};

// Esta funcion devuelve traducciones específicas para la página
export const langUser = (page) => {
  const prefLang = verifyLang();

  const translaLocale = languages[prefLang.value] || languages["en"];

  // Retorna traducciones específicas para la página o un objeto vacío si no se encuentran
  return translaLocale[page] || {};
};

"use client";

import { useEffect, useState, useRef } from "react";
import Image from "next/image";

// Componentes
import CardProject from "@/components/ListProject/CardProject";
import { TagSelected, desactivateCate } from "./CategoriesList";

// Estilos
import s from "./page.module.css";

// Recursos
import IconReLoad from "@/components/icons/IconReLoad";
import IconAdvice from "@/img/icons/iconAdvise.svg";
import IconLoadMore from "@/components/icons/IconLoadMore";

// Métodos exportados para el uso externo
export let handleCategorySelect;
export let handleSearchChange;
export let selectedCate;
export let searchTermValue;

const ProjectsList = ({ langCategories, langTitle }) => {
  //? Sistema de idiomas - Asi funciona la consulta del idioma
  const { titleList, defaultList, forList, errorInfo, btnSearchMore } = langTitle;

  const [projects, setProjects] = useState([]);

  // Estados de categoría y términos de búsqueda escrita
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedCategory, setSelectedCategory] = useState(null);

  // Estado de carga de datos
  const [loadingState, setLoadingState] = useState({
    loading: true,
    error: false,
  });

  // prueba de codigo
  const [currentBlock, setCurrentBlock] = useState(1);

  const [loadingMore, setLoadingMore] = useState(false);
  // Mostrar el botón solo si hay más proyectos para cargar
  const showLoadMoreButton = currentBlock !== null;

  // Referencia para auto-scrolling
  const elementRef = useRef(null);
  function handleClickToScrollEle() {
    if (elementRef.current) {
      elementRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  }

  // Petición inicial para obtener todos los proyectos
  useEffect(() => {
    (async () => {
      let response = await fetch("/api/projectUtils?page=1", {
        method: "GET",
      });

      if (response.ok) {
        let data = await response.json();
        setLoadingState({ loading: false, error: false });
        setProjects(data.projects);
      } else {
        setLoadingState({ loading: false, error: true });
        console.error("Datos no bro :(");
      }
    })();
  }, []);

  // Exportar el estado de categoría y términos de búsqueda
  selectedCate = selectedCategory;
  searchTermValue = searchTerm;

  //* Método para actualizar el estado desde otros componentes
  handleCategorySelect = async (category) => {
    if (searchTerm) {
      handleSearchChange(null);
      setSearchTerm("");
    }

    handleClickToScrollEle();
    setSelectedCategory(category);
    setCurrentBlock(1); // Reset currentBlock to 1
    setProjects([]); // Reset projects to empty array
    setLoadingState({ loading: true, error: false });

    const block = 1;
    const queryString = category
      ? `/api/projectUtils?tag=${category}&block=${block}`
      : `/api/projectUtils?page=1`;

    try {
      const response = await fetch(queryString, {
        method: "GET",
      });

      const data = await response.json();

      if (response.ok) {
        setLoadingState({ loading: false, error: false });
        setProjects(data.projects);

        // Check if the number of returned projects is less than the block size
        if (data.projects.length < 2) {
          setCurrentBlock(null); // No more projects to load
        }
      } else {
        if (response.status === 404) {
          setLoadingState({ loading: false, error: false });
          setProjects([]);
          setCurrentBlock(null); // No more projects to load
        } else if (response.status === 500) {
          setLoadingState({ loading: false, error: true });
          setProjects([]);
        }
      }
    } catch (error) {
      setLoadingState({ loading: false, error: true });
      console.error("Error al hacer la petición a la API:", error);
      setProjects([]);
    }
  };

  //* Metodo para actualizar el estado de búsqueda desde otros componentes
  handleSearchChange = async (term) => {
    if (selectedCategory) {
      desactivateCate(null);
      setSelectedCategory(null);
    }

    if (term === null || term === undefined) {
      return;
    }

    const value = term.trim();
    if (!value) {
      return;
    }

    handleClickToScrollEle();
    setSearchTerm(term);
    setCurrentBlock(1); // Reset currentBlock to 1
    setProjects([]); // Reset projects to empty array
    setLoadingState({ loading: true, error: false });

    const block = 1;
    const queryString =
      searchTerm || term
        ? `/api/projectUtils?search=${term}&block=${block}`
        : `/api/projectUtils?page=1`;

    try {
      const response = await fetch(queryString, {
        method: "GET",
      });

      const data = await response.json();

      if (response.ok) {
        setLoadingState({ loading: false, error: false });
        setProjects(data.projects);

        // Check if the number of returned projects is less than the block size
        if (data.projects.length < 2) {
          setCurrentBlock(null); // No more projects to load
        }
      } else {
        if (response.status === 404) {
          setLoadingState({ loading: false, error: false });
          setProjects([]);
          setCurrentBlock(null); // No more projects to load
        } else if (response.status === 500) {
          setLoadingState({ loading: false, error: true });
          setProjects([]);
        }
      }
    } catch (error) {
      setLoadingState({ loading: false, error: true });
      console.error("Error en la búsqueda:", error);
      setProjects([]);
    }
  };

  // Contenedor de busqueda actual
  const TagSearched = () => {
    return (
      <div className={s.tag_searched}>
        {` ${forList}:`} "<p>{searchTerm}</p>"
      </div>
    );
  };

  // Skeleton loader - Componente de cargando
  const SkeletonLoader = () => {
    return (
      <>
        <li className={s.ske_card}>
          <div />
        </li>
        <li className={s.ske_card}>
          <div />
        </li>
        <li className={s.ske_card}>
          <div />
        </li>
        <li className={s.ske_card}>
          <div />
        </li>
      </>
    );
  };

  // Consult error - Componente de consulta fallida
  const CompError = () => {
    return (
      <div className={s.cont_error}>
        <span>{":("}</span>
        <p>{errorInfo.errorServer}</p>
        <button
          className={s.btn_reload}
          onClick={() => window.location.reload()}
        >
          <p>{errorInfo.btnReload}</p>
          <IconReLoad color="var(--cl-btn-err)" />
        </button>
      </div>
    );
  };

  // Consult error categorie - Componente de consulta fallida de categorias
  const CompErrorSearch = () => {
    return (
      <div className={s.cont_error}>
        <Image src={IconAdvice} alt="Advice" />
        <p>{errorInfo.notFound}</p>
      </div>
    );
  };

  const handleLoadMore = async () => {
    setLoadingMore(true);

    const queryString = selectedCategory
      ? `/api/projectUtils?tag=${selectedCategory}&block=${currentBlock + 1}`
      : searchTerm
      ? `/api/projectUtils?search=${searchTerm}&block=${currentBlock + 1}`
      : `/api/projectUtils?page=${currentBlock + 1}`;

    try {
      const response = await fetch(queryString, {
        method: "GET",
      });

      const data = await response.json();

      if (response.ok) {
        setLoadingMore(false);
        setProjects((prevProjects) => [...prevProjects, ...data.projects]);

        // Check if the number of returned projects is less than the block size
        if (data.projects.length < 6) {
          setCurrentBlock(null); // No more projects to load
        } else {
          setCurrentBlock(currentBlock + 1);
        }
      } else {
        if (response.status === 404) {
          setLoadingMore(false);
          setLoadingState({ loading: false, error: false });
          setCurrentBlock(null); // No more projects to load
        } else if (response.status === 500) {
          setLoadingMore(false);
          setLoadingState({ loading: false, error: true });
          console.error("Error al hacer la petición a la API:", error);
        }
      }
    } catch (error) {
      setLoadingMore(false);
      setLoadingState({ loading: false, error: true });
      console.error("Error al hacer la petición a la API:", error);
    } finally {
      setLoadingMore(false);
    }
  };

  return (
    <>
      <div className={s.cont_title_sec_2} ref={elementRef}>
        <h2 className={s.title_sec_2}>
          {titleList}
          {selectedCategory ? (
            <TagSelected />
          ) : searchTerm ? (
            <TagSearched />
          ) : (
            ` ${defaultList}:`
          )}
        </h2>
      </div>

      <div className={s.cont_projects}>
        {loadingState.loading && !loadingMore ? (
          <ul className={s.cont_projects_list} style={{ padding: "0.5rem" }}>
            <SkeletonLoader />
          </ul>
        ) : loadingState.error ? (
          <CompError />
        ) : projects.length > 0 ? (
          <>
            <ul className={s.cont_projects_list}>
              {/* //! Pinta la informacion en reversa (POR AHORA FUNCIONA DE ESTE MODO) */}
              {[...projects].map((project) => (
                <CardProject info={project} langCategories={langCategories} key={project.id} />
              ))}
            </ul>
            {!loadingMore && showLoadMoreButton && (
              <div className={s.cont_load_more}>
                <button
                  className={s.btn_load_more}
                  style={{ display: `${showLoadMoreButton ? "flex" : "none"}` }}
                  onClick={handleLoadMore}
                  disabled={loadingState.loading || loadingMore}
                >
                  {btnSearchMore}
                  <IconLoadMore color="var(--color-intern)" />
                </button>
              </div>
            )}
            {loadingMore && (
              <ul
                className={s.cont_projects_list}
                style={{ padding: "0.5rem" }}
              >
                <SkeletonLoader />
              </ul>
            )}
          </>
        ) : (
          <CompErrorSearch />
        )}
      </div>
    </>
  );
};

export default ProjectsList;

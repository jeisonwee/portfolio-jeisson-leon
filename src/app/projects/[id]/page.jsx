/* --------------------------------------------------- */
/* Pagina - Plantilla para cada proyecto ------------- */
/* --------------------------------------------------- */

// import { getProjectByIdentifier } from "@/db/queries"
import { langUser } from "@/scripts/langConfig";
import Image from "next/image";

// Componentes
import AuthorsPic from "./AuthorsPic";
import DynamicSlider from "./DynamicSlider";
import SliderImages from "@/components/HomeComps/SliderImages";
import VideoPlayer from "@/components/video/VideoPlayer";
import FooterMain from "@/components/FooterMain";

// Estilos
import s from "./page.module.css";

// Recursos
import BackPatternProject from "@/components/images/BackPatternProject";
import IconDownload from "@/components/icons/IconDownload";
import IconArrow from "@/components/icons/IconArrowLeft";
import IconGitlab from "@/components/icons/IconGitlab";
import IconLinux from "@/img/icons/icon_linux.svg";
import IconMacOS from "@/img/icons/icon_apple.svg";
import IconDesktop from "@/components/icons/IconDesktop";
import IconFeedback from "@/components/icons/IconFeedback";
import IconDonate from "@/components/icons/IconDonate";
import IconClock from "@/components/icons/IconClock";

export default async function ProjectDetails({ params }) {
  //? Sistema de idiomas - Asi funciona la consulta del idioma
  const { categories, projectDetails } = langUser("projects");

  //! Esta es informacion de prueba para maquetar el diseño proyecto
  let projectData = {
    brand: {
      color_main: "#00FF95",
    },
    logo: "/projects-source/logo_autocommit.png",
    images: [
      "/projects-source/autocommit_demo.png",
      "/projects-source/autocommit_demo.png",
      "/projects-source/autocommit_demo.png",
    ],
    title: "Autocommit AI (Beta Alpha)",
    description:
      "Simplifica el proceso de commits de Git generando mensajes de commit significativos y descriptivos mediante IA, automatizando los commits y pushes en poco tiempo, manteniendo un historial de commits limpio y ordenado.",
    authors: [
      {
        full_name: "Jeisson León",
        profile_image: "/img/pic_profile.png",
      },
    ],
    organizations: [
      {
        name: "Lion Labs",
        pic: "/img/pro/pic_lionlabs.jpeg",
      },
    ],
    buttons: {
      download: "Descargar Autocommit",
      repository: {
        label: "Repositorio de código",
        platform: "Gitlab",
      },
    },
    availability: "Disponible para Linux y MacOS, pronto para Windows",
    details: {
      published_date: "2024-07-26",
      version: "v0.7.3-beta",
      status: {
        state: "active",
        label: "Beta",
      },
      technologies: ["Python 3", "Bash", "Git"],
      compatibility: ["MacOs", "Linux"],
      license: "Apache License 2.0",
      documentation: "Documentación de uso",
      location: "Bogotá D.C, COL",
    },
    tags: ["PYTHON", "AUTOMATION", "AI_ML", "SOFTWARE", "EXPERIMENTS", "LINUX"],
    information: {},
  };
  //? COMPONENTES DE INSERCION PARA PLANTILLA
  // Seccion 2
  const ComponentSeccion2 = () => {
    return (
      <section className={s.cont_study}>
        <section className={s.rules_info}>
          <h4>Rendimiento y productividad al maximo, sin aburrirte</h4>
        </section>
        <div className={s.study_info}>
          <div className={s.study_stadistic}>
            <div className={s.refer_cont}>
              <div className={s.cont_bar}>
                <h3>Autocommit</h3>
                <div className={`${s.bar_style} ${s.autocommit_bar}`}>
                  80 min
                </div>
                <p>16 commits creados</p>
              </div>
              <div className={s.cont_bar}>
                <h3>Commits manuales</h3>
                <div className={s.cont_manual_bars}>
                  <div className={`${s.bar_style} ${s.manual_bar}`}>56 min</div>
                  <div className={`${s.bar_style} ${s.bad_bar}`}>
                    24 min <p>Tiempo creando commits</p>
                  </div>
                </div>
                <p>16 commits creados</p>
              </div>
              <div className={s.bars}>
                <span>
                  <p>0</p>
                </span>
                <span>
                  <p>10</p>
                </span>
                <span>
                  <p>20</p>
                </span>
                <span>
                  <p>30</p>
                </span>
                <span>
                  <p>40</p>
                </span>
                <span>
                  <p>50</p>
                </span>
                <span>
                  <p>60</p>
                </span>
                <span>
                  <p>70</p>
                </span>
                <span>
                  <p>80</p>
                </span>
              </div>
            </div>
            <section className={s.info_bottom}>
              <p>
                <IconClock color="var(--color-contrast-700-t)" />
                Tiempo de desarrollo total en minutos
              </p>
            </section>
          </div>
          <div className={s.study_explain}>
            <section>
              <p>100% de efectividad</p>
              <span>80 min de desarrollo</span>
            </section>
            <section>
              <p>70% de eficiencia</p>
              <span>56 min de desarrollo</span>
            </section>
          </div>
        </div>
      </section>
    );
  };
  // Seccion 3
  const listWrong = [
    "fix cosas varias",
    "ultimos cambios del dia bro",
    "no se solo cambie un color",
    "ya arreglo lo otro de lo otro",
    "no me pagan pa pensar esto bro",
  ];
  const ComponentSeccion3 = ({ listElements }) => {
    return (
      <ul className={s.list_wrong}>
        {listElements.map((item, index) => (
          <li key={index}>
            <svg
              width={14}
              height={14}
              viewBox="0 0 14 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1.707.793A1 1 0 0 0 .293 2.207L5.586 7.5.293 12.793a1 1 0 1 0 1.414 1.414L7 8.914l5.293 5.293a1 1 0 0 0 1.414-1.414L8.414 7.5l5.293-5.293A1 1 0 1 0 12.293.793L7 6.086z"
                fill="red"
              />
            </svg>
            "{item}"
          </li>
        ))}
      </ul>
    );
  };
  const ComponentSeccion3_2 = () => {
    return (
      <div className={s.cont_errors}>
        <div>
          <svg
            width={33}
            height={32}
            viewBox="0 0 33 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M24.77 2.322A1.25 1.25 0 0 0 23.484.178l-3.395 2.037a7.9 7.9 0 0 0-3.337-.738 7.9 7.9 0 0 0-3.338.739L10.02.178a1.25 1.25 0 0 0-1.287 2.144l2.442 1.465a7.86 7.86 0 0 0-2.31 5.578v.341a7 7 0 0 0-.921.599 1.3 1.3 0 0 0-.316-.184l-2.95-1.18a1.25 1.25 0 1 0-.929 2.321l2.453.981a7 7 0 0 0-1.026 3.667v1.795H1.999a1.25 1.25 0 1 0 0 2.5h3.176v.222c0 1.855.436 3.609 1.212 5.163l-2.639 1.056a1.25 1.25 0 1 0 .929 2.32l2.95-1.18q.07-.028.136-.063a11.55 11.55 0 0 0 8.987 4.28c3.628 0 6.866-1.67 8.989-4.281q.066.036.138.065l2.95 1.18a1.25 1.25 0 0 0 .928-2.321l-2.64-1.057a11.5 11.5 0 0 0 1.21-5.162v-.222h3.176a1.25 1.25 0 0 0 0-2.5h-3.175V15.91a7 7 0 0 0-1.025-3.666l2.454-.982a1.25 1.25 0 1 0-.928-2.321l-2.95 1.18q-.177.072-.318.185a7 7 0 0 0-.92-.598v-.343c0-2.179-.882-4.15-2.31-5.578zm1.056 16.599v1.506a9.075 9.075 0 0 1-7.828 8.99v-8.99a1.25 1.25 0 0 0-2.5 0v8.99a9.08 9.08 0 0 1-7.823-8.99V15.91a4.56 4.56 0 0 1 4.558-4.558h9.035a4.56 4.56 0 0 1 4.558 4.558zM16.752 3.977a5.39 5.39 0 0 1 5.368 4.926 7 7 0 0 0-.852-.051h-9.035q-.432 0-.85.05a5.39 5.39 0 0 1 5.369-4.925"
              fill="red"
            />
          </svg>
          <p>Errores costosos en proyectos colaborativos.</p>
        </div>
        <span className={s.comp_ry} />
        <div>
          <svg
            width={32}
            height={32}
            viewBox="0 0 32 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M5.866.51c.681-.68 1.703-.68 2.383 0 .681.681.681 1.703 0 2.384L7.058 4.085l1.191 1.192c.681.68.681 1.702 0 2.383-.34.34-.85.51-1.191.51-.34 0-.851-.17-1.192-.51L4.675 6.468 3.483 7.66c-.34.34-.85.51-1.191.51-.34 0-.851-.17-1.192-.51-.68-.681-.68-1.703 0-2.383l1.192-1.192L1.1 2.894C.42 2.213.42 1.19 1.1.51c.681-.681 1.703-.681 2.383 0l1.192 1.191zm0 11.915c.681-.681 1.703-.681 2.383 0 .681.68.681 1.702 0 2.383l-1.191 1.191 1.191 1.192c.681.68.681 1.702 0 2.383-.34.34-.85.51-1.191.51-.34 0-.851-.17-1.192-.51l-1.191-1.192-1.192 1.192c-.34.34-.85.51-1.191.51-.34 0-.851-.17-1.192-.51-.68-.681-.68-1.703 0-2.383l1.192-1.192L1.1 14.808c-.68-.681-.68-1.702 0-2.383.681-.681 1.703-.681 2.383 0l1.192 1.191zM8.25 24.34c-.68-.68-1.702-.68-2.383 0l-1.191 1.192-1.192-1.192c-.68-.68-1.702-.68-2.383 0-.68.681-.68 1.703 0 2.383l1.192 1.192L1.1 29.107c-.68.681-.68 1.703 0 2.383.34.34.852.51 1.192.51s.851-.17 1.191-.51l1.192-1.19 1.191 1.191c.34.34.852.51 1.192.51s.851-.17 1.191-.51c.681-.68.681-1.702 0-2.383l-1.191-1.191 1.191-1.192c.681-.68.681-1.702 0-2.383M30.207 5.787H16.59c-1.021 0-1.702-.68-1.702-1.702s.68-1.702 1.702-1.702h13.617c1.021 0 1.702.68 1.702 1.702s-.68 1.702-1.702 1.702m0 8.51H16.59c-1.021 0-1.702.68-1.702 1.702s.68 1.702 1.702 1.702h13.617c1.021 0 1.702-.68 1.702-1.702s-.68-1.702-1.702-1.702M16.59 26.214h13.617c1.021 0 1.702.68 1.702 1.702 0 1.02-.68 1.702-1.702 1.702H16.59c-1.021 0-1.702-.681-1.702-1.702 0-1.022.68-1.703 1.702-1.703"
              fill="red"
            />
          </svg>
          <p>Pérdida de contexto en tu propio trabajo.</p>
        </div>
      </div>
    );
  };
  const ComponentSeccion4 = () => {
    return (
      <ul className={s.cont_list_fea}>
        <li>
          <svg
            width={24}
            height={29}
            viewBox="0 0 24 29"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M7.5 1.5A1.5 1.5 0 0 1 9 0h6a1.5 1.5 0 0 1 0 3h-1.5v1.593C19.42 5.33 24 10.38 24 16.5c0 6.627-5.373 12-12 12s-12-5.373-12-12C0 10.38 4.58 5.331 10.5 4.593V3H9a1.5 1.5 0 0 1-1.5-1.5m13.5 15a9 9 0 1 1-18 0 9 9 0 0 1 18 0M2.135 3.271c.663-.494 1.59-.268 2.022.437s.205 1.62-.448 2.128q-.493.384-.948.81c-.604.566-1.543.648-2.172.111-.63-.536-.709-1.487-.117-2.065q.784-.766 1.663-1.421m19.73 0c-.663-.494-1.59-.268-2.022.437s-.205 1.62.448 2.128q.493.384.948.81c.604.566 1.543.648 2.172.111.63-.536.709-1.487.117-2.065a16.5 16.5 0 0 0-1.663-1.421m-5.054 10.543a1.5 1.5 0 1 0-2.121-2.121l-1.913 1.912a3 3 0 1 0 2.121 2.121z"
              fill="var(--brand-color)"
            />
          </svg>
          <p>Realiza commits automáticamente en intervalos definidos.</p>
        </li>
        <li>
          <svg
            width={24}
            height={25}
            viewBox="0 0 24 25"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M6.084 1.75a1.25 1.25 0 1 0-2.5 0V15a4.837 4.837 0 0 0 1.25 9.505 4.84 4.84 0 0 0 4.685-3.634A12 12 0 0 0 20.372 10.02a4.837 4.837 0 0 0-1.2-9.52 4.835 4.835 0 0 0-1.31 9.49 9.504 9.504 0 0 1-8.372 8.37A4.84 4.84 0 0 0 6.084 15zm1.085 17.91a2.335 2.335 0 1 0-4.669.02 2.335 2.335 0 0 0 4.669-.02M19.214 7.67h-.083a2.335 2.335 0 1 1 .083 0"
              fill="var(--brand-color)"
            />
          </svg>
          <p>
            Genera mensajes claros y estandarizados basados en palabras clave
            relevantes para tu proyecto.
          </p>
          <span className={s.ry_fea} />
          <div className={s.cont_compare}>
            <div>
              <h4>Commit de Autocommit:</h4>
              <img
                src="/projects-source/commit-autocommit.png"
                style={{ border: "1px solid green" }}
                alt=""
              />
            </div>
            <div>
              <h4>Commit del Desarrollador:</h4>
              <img
                src="/projects-source/commit-manual.png"
                style={{ border: "1px solid red" }}
                alt=""
              />
            </div>
          </div>
        </li>
        <li>
          <svg
            width={24}
            height={26}
            viewBox="0 0 24 26"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M11.692 2.965c5.438 0 9.846 4.409 9.846 9.847s-4.408 9.846-9.846 9.846c-3.854 0-7.126-2.217-8.744-5.471a1.23 1.23 0 0 0-2.204 1.095c2.008 4.04 6.104 6.837 10.948 6.837C18.49 25.12 24 19.61 24 12.812S18.49.504 11.692.504c-3.72 0-7 1.65-9.23 4.245V2.965a1.23 1.23 0 0 0-2.462 0V7.89a1.23 1.23 0 0 0 1.234 1.23l.544-.001q.071.003.142 0l4.237-.013a1.23 1.23 0 1 0-.007-2.462l-2.067.006c1.79-2.252 4.511-3.684 7.61-3.684m1.231 4.924a1.23 1.23 0 0 0-2.461 0v4.413c0 .653.26 1.279.72 1.74l2.102 2.101a1.23 1.23 0 1 0 1.74-1.74l-2.1-2.101z"
              fill="var(--brand-color)"
            />
          </svg>
          <p>
            Mantiene un historial limpio y bien documentado, perfecto para
            revisiones de código y colaboración.
          </p>
        </li>
      </ul>
    );
  };

  /* try {
    const projectDatatest = await getProjectByIdentifier(params.id);
    console.log(projectDatatest);
  } catch (error) {
    console.error(error.message);
  } */

  const ErrorProject = () => {
    return <article>No existe el proyecto que buscas</article>;
  };

  const ProjectInformation = () => {
    const ContAuthors = () => {
      return (
        <div className={s.cont_authors}>
          <AuthorsPic
            authors={projectData.authors}
            organizations={projectData.organizations}
          />

          <p className={s.tx_authors}>
            {projectData.authors.map((item, index) => (
              <strong key={index}>{item.full_name}</strong>
            ))}{" "}
            con{" "}
            {projectData.organizations.map((item, index) => (
              <strong key={index}>{item.name}</strong>
            ))}
          </p>
        </div>
      );
    };

    // Estructura de imagenes predeterminadas
    const osImages = {
      MacOs: IconMacOS,
      Linux: IconLinux,
    };

    // Detalles del proyecto
    const DetailsComponent = ({ details }) => {
      const COLOR_MAP_STATE = {
        active: "#007bff", // Azul para Activo
        archived: "#6c757d", // Gris para Archivado
        pending: "#ffc107", // Naranja para Pendiente
        deprecated: "#dc3545", // Rojo para Obsoleto
      };

      const COLOR_MAP_LABEL = {
        beta: "#28a745", // Verde para Beta
        alpha: "#ff5733", // Naranja para Alpha
        stable: "#007bff", // Azul para Stable
        experimental: "#f0ad4e", // Amarillo para Experimental
      };

      return (
        <ul className={s.details_container}>
          {details.published_date && (
            <li className={s.details_item}>
              <p>Fecha de publicación:</p>
              <span>{details.published_date}</span>
            </li>
          )}

          {details.version && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Versión:</p> <span>{details.version}</span>
              </li>
            </>
          )}

          {details.status && details.status.state && details.status.label && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Estado:</p>
                <div className={s.cont_tags_state}>
                  <span
                    style={{
                      "--color-tag":
                        COLOR_MAP_STATE[details.status.state] || "red", // Color para el estado
                    }}
                  >
                    {projectDetails.status_map[details.status.state] ||
                      details.status.state}
                  </span>
                  <span
                    style={{
                      "--color-tag":
                        COLOR_MAP_LABEL[details.status.label.toLowerCase()] ||
                        "black", // Color para la etiqueta (label)
                    }}
                  >
                    {details.status.label}
                  </span>
                </div>
              </li>
            </>
          )}

          {details.technologies && details.technologies.length > 0 && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Tecnologías:</p>{" "}
                <span>{details.technologies.join(", ")}</span>
              </li>
            </>
          )}

          {details.compatibility && details.compatibility.length > 0 && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Compatibilidad:</p>{" "}
                <span>{details.compatibility.join(", ")}</span>
              </li>
            </>
          )}

          {details.license && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Licencia:</p> <span>{details.license}</span>
              </li>
            </>
          )}

          {details.documentation && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Documentación:</p>{" "}
                <a
                  href={details.documentation}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {details.documentation}
                </a>
              </li>
            </>
          )}

          {details.location && (
            <>
              <div className={s.ry_info} />
              <li className={s.details_item}>
                <p>Ubicación:</p> <span>{details.location}</span>
              </li>
            </>
          )}
        </ul>
      );
    };

    // Tag categorias
    const TagAssigned = ({ name }) => {
      return <span className={s.tag_assigned}>{categories[name]}</span>;
    };

    return (
      <>
        <BackPatternProject className={s.background_img} color="#808080" />

        <article className={`${s.sec_1} ${s.seccion}`}>
          <svg
            width={652}
            height={506}
            className={s.svg_brand}
            viewBox="0 0 652 506"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <g opacity={0.5}>
              <g filter="url(#filter0_f_1838_218)">
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M319.518 305.894C377.621 307.039 431.956 270.421 448.048 214.659C462.811 163.502 426.831 116.025 383.1 85.5703C342.465 57.272 289.559 47.6289 249.008 76.0468C206.521 105.821 190.509 160.324 205.712 209.882C221.793 262.304 264.624 304.812 319.518 305.894Z"
                  fill="url(#paint0_linear_1838_218)"
                  fillOpacity={0.72}
                />
              </g>
            </g>
            <defs>
              <filter
                id="filter0_f_1838_218"
                x={0.0878906}
                y={-141.057}
                width={651.328}
                height={646.977}
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
              >
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feBlend
                  mode="normal"
                  in="SourceGraphic"
                  in2="BackgroundImageFix"
                  result="shape"
                />
                <feGaussianBlur
                  stdDeviation={100}
                  result="effect1_foregroundBlur_1838_218"
                />
              </filter>
              <linearGradient
                id="paint0_linear_1838_218"
                x1={325.752}
                y1={305.92}
                x2={325.752}
                y2={58.9439}
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor="var(--brand-color)" />
                <stop offset={1} stopColor="var(--brand-color)" />
              </linearGradient>
            </defs>
          </svg>

          <h3 className={s.tx_tag}>PROYECTO</h3>
          <div className={s.sec1_first}>
            <Image
              src={projectData.logo}
              className={s.img_logo}
              width={56}
              height={56}
              alt="logo"
            />
            <h1 className={s.tx_title}>{projectData.title}</h1>
            <p className={s.tx_description}>{projectData.description}</p>
            <ContAuthors />
          </div>

          <div className={s.cont_options_source}>
            <div className={s.cont_btns}>
              <button className={s.btn_download}>
                <div className={s.cont_down_info}>
                  <IconDownload color="var(--color-back)" />
                  {projectData.buttons.download}
                </div>
                <div className={s.cont_arrow}>
                  <IconArrow
                    color="var(--color-back)"
                    style={{ transform: "rotate(180deg)" }}
                  />
                </div>
              </button>
              <button className={s.btn_repo}>
                <div>
                  {projectData.buttons.repository.platform === "Gitlab" && (
                    <IconGitlab color="var(--color-contrast-700-t)" />
                  )}
                  {projectData.buttons.repository.label}
                </div>
                <IconArrow
                  color="var(--color-contrast-700-t)"
                  style={{ transform: "rotate(180deg)" }}
                />
              </button>
            </div>
            <p className={s.tx_down_foo}>
              <span>
                {projectData.details.compatibility.map((os) => (
                  <Image
                    key={os}
                    src={osImages[os]}
                    alt={"logo"}
                    width={14}
                    height={14}
                  />
                ))}
              </span>
              {projectData.availability}
            </p>
          </div>

          <DetailsComponent details={projectData.details} />

          <div className={s.cont_tags}>
            {projectData.tags.map((tag) => (
              <TagAssigned name={tag} key={tag} />
            ))}
          </div>

          <DynamicSlider>
            <SliderImages images={projectData.images} />
          </DynamicSlider>

          <div className={s.cont_details_options}>
            <div className={s.btn_deta_op}>
              <div className={s.cont_ico}>
                <IconDesktop color="var(--color-contrast-700-t)" />
              </div>
              <p>Solo para escritorio</p>
            </div>
            <div className={s.btn_deta_op}>
              <div className={s.cont_ico}>
                <IconFeedback color="var(--color-contrast-700-t)" />
              </div>
              <p>FeedBack y soporte</p>
            </div>
            <div className={s.btn_deta_op}>
              <div className={s.cont_ico}>
                <span style={{ color: "green" }}>TODOS</span>
              </div>
              <p>Clasificacion por edad</p>
            </div>
            <div className={s.btn_deta_op}>
              <div className={s.cont_ico}>
                <IconDonate color="var(--color-contrast-700-t)" />
              </div>
              <p>Invitame un trago</p>
            </div>
          </div>
        </article>

        <div className={s.ry_inf} />

        <article className={`${s.sec_2} ${s.seccion}`}>
          <section className={s.rules_info}>
            <h2>
              Elimina la molestia de hacer commits manuales, automatiza tu flujo
              de trabajo
            </h2>
            <p>
              Imagina que ya no tienes que detenerte cada <strong>5</strong>,
              <strong>30 minutos</strong>, o más, para escribir{" "}
              <strong>manualmente</strong> qué cambios hiciste, ni revisar
              minuciosamente tu código para detallar el mensaje de commit 🥱😴.
            </p>
          </section>
          <ComponentSeccion2 />
        </article>

        <div className={s.ry_inf} />

        <article className={`${s.sec_3} ${s.seccion}`}>
          <section className={s.rules_info}>
            <h3>¿Cual es el problema?</h3>
            <p>
              Haces cambios significativos en tu código y piensas: "
              <strong>Luego hago el commit</strong>", “
              <strong>Voy por un cafe y subo los cambios...</strong>”.
            </p>
            <p>
              Pasa el tiempo, y el historial del proyecto se convierte en un
              caos porque olvidaste registrar esos pequeños pasos. O peor, te
              ves escribiendo mensajes genéricos como:
            </p>
          </section>
          <ComponentSeccion3 listElements={listWrong} />
          <section className={s.rules_info}>
            <p>
              La repetición de tareas manuales como esta no solo es tediosa,
              sino que puede llevar a:
            </p>
          </section>
          <ComponentSeccion3_2 />
        </article>

        <div className={s.ry_inf} />

        <article className={`${s.sec_4} ${s.seccion}`}>
          <section className={s.rules_info}>
            <h2>
              <strong style={{ color: "var(--brand-color)" }}>
                Autocommit
              </strong>{" "}
              es tu ayudante autonomo, no necesita supervision
            </h2>
            <p>
              AI elimina el <strong>tedio</strong> y la{" "}
              <strong>incertidumbre</strong> en la gestión de commits,
              permitiéndote enfocarte en lo que realmente importa:{" "}
              <strong>crear</strong>, <strong>desarrollar</strong> y{" "}
              <strong>resolver problemas</strong>.
            </p>
            <p>
              Con solo instalar y ejecutar <strong>Autocommit</strong>:
            </p>
          </section>
          <ComponentSeccion4 />
          <section className={s.rules_info}>
            <p>
              Tu línea de tiempo estará siempre organizada sin que tengas que
              mover un dedo.
            </p>
          </section>
          <VideoPlayer
            style={{ margin: "1rem 0" }}
            src="https://raw.githubusercontent.com/JeissonWeeDev/portfolio-jeisson-leon/main/video/Video-prueba-autocommit.mp4"
            controls
            loop
          />
        </article>
      </>
    );
  };

  return (
    <section
      className={s.page_container}
      style={{ "--brand-color": projectData.brand.color_main }}
    >
      {/* Escepcion de error */}
      {projectData ? null : <ErrorProject />}

      {/* Contenido del proyecto */}
      {projectData && <ProjectInformation />}

      <FooterMain />
    </section>
  );
}

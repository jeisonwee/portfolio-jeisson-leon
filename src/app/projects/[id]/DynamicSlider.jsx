"use client";

import { useEffect, useRef, useState } from "react";

// Estilos
import s from "./page.module.css";

const SliderContainer = ({ children }) => {
  const containerRef = useRef(null);
  const [containerHeight, setContainerHeight] = useState(0);

  useEffect(() => {
    const calculateHeight = () => {
      if (containerRef.current) {
        const width = containerRef.current.offsetWidth; // Obtén el ancho del contenedor
        const height = (width / 16) * 9; //? Calcula la altura con el aspect ratio de 16:9
        setContainerHeight(height);
      }
    };

    calculateHeight();

    window.addEventListener("resize", calculateHeight);
    return () => window.removeEventListener("resize", calculateHeight);
  }, []);

  return (
    <div
      className={s.cont_slider}
      ref={containerRef}
      style={{
        width: "100%",
        height: `${containerHeight}px`,
        position: "relative",
      }}
    >
      {children}
    </div>
  );
};

export default SliderContainer;

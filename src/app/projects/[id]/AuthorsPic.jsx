"use client";

import Image from "next/image";

// Estilos
import s from "./page.module.css";

export default function AuthorsPic({ authors, organizations }) {
  return (
    <div className={s.cont_authors_pic}>
      {authors && authors.map((item, index) => (
        <Image
          key={index}
          src={item.profile_image}
          width={24}
          height={24}
          alt="img"
          style={{ zIndex: 1 - index }}
        />
      ))}
      {organizations && organizations.map((item, index) => (
        <Image
          key={`org-${index}`}
          src={item.pic}
          width={24}
          height={24}
          alt="img"
          style={{ zIndex: -1 - index }}
        />
      ))}
    </div>
  );
}

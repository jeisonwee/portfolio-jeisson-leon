"use client";

import { useState, useRef } from "react";
import { handleCategorySelect } from "./ProjectList";

// Estilos
import s from "./page.module.css";

// Recursos
import IconX from "@/components/icons/IconX";
import IconArrowLeft from "@/components/icons/IconArrowLeft";

export let TagSelected;
export let desactivateCate;

const CategoriesList = ({ titleCate ,categories, categoriesIndex }) => {
  const [selectedCategory, setSelectedCategory] = useState(null);

  // Estado de modal para mobiles
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Exportar el metodo de desactivacion de categoria
  desactivateCate = setSelectedCategory;

  // Referencia de elemento para scroll automatico
  const elementRef = useRef(null);

  const handleClickToScrollEle = () => {
    if (elementRef.current) {
      elementRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  };

  const handleSelect = (category) => {
    if (selectedCategory === category) {
      handleCategorySelect(null);
      setSelectedCategory(null);
    } else {
      handleClickToScrollEle();
      setSelectedCategory(category);
      handleCategorySelect(category);
    }
  };

  // Tag de seleccion de categoria actual
  TagSelected = () => {
    return (
      <button
        className={s.tag_selected_cate}
        onClick={() => handleSelect(null)}
      >
        <p>{categories[selectedCategory]}</p>
        <IconX color="var(--color-back)" width={10} height={10} />
      </button>
    );
  };

  return (
    <>
      <div className={s.cont_categories} ref={elementRef}>
        {categoriesIndex.map((category, index) => (
          <div
            key={index}
            className={`${s.btn_link_filter} ${
              selectedCategory === category ? s.selectedFilter : ""
            }`}
            onClick={() => handleSelect(category)}
          >
            {categories[category]}
          </div>
        ))}
      </div>

      <div className={s.cont_categories_mobi}>
        <button
          className={s.btn_cate_menu}
          onClick={() => setIsModalOpen(!isModalOpen)}
        >
          {titleCate}
          <IconArrowLeft
            color="var(--color-contrast-700-t)"
            style={{ transform: "rotate(180deg)" }}
          />
        </button>
        {/* Despligue de modal para mobile */}
        {isModalOpen && (
          <>
            <div className={s.modal_content}>
              <section className={s.modal_head}>
                <h5>{titleCate}</h5>
                <button
                  className={s.btn_close_float}
                  onClick={() => setIsModalOpen(!isModalOpen)}
                >
                  <IconX color="var(--cl-x)" />
                </button>
              </section>
              <section className={s.modal_body}>
                {categoriesIndex.map((category, index) => (
                  <div
                    key={index}
                    className={`${s.btn_link_filter} ${
                      selectedCategory === category ? s.selectedFilter : ""
                    }`}
                    onClick={() => {
                      setIsModalOpen(!isModalOpen);
                      handleSelect(category);
                    }}
                  >
                    {categories[category]}
                  </div>
                ))}
              </section>
            </div>
            <div
              className={s.modal_container}
              onClick={() => setIsModalOpen(!isModalOpen)}
            />
          </>
        )}
      </div>
    </>
  );
};

export default CategoriesList;

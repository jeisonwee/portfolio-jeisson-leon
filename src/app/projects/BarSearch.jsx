"use client";

import { useState, useRef } from "react";
import { handleSearchChange } from "./ProjectList";

// Estilos
import s from "./page.module.css";

// Recursos
import IconSearch from "@/components/icons/IconSearch";

const IconArrowRight = () => {
  return (
    <svg
      width={16}
      height={16}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.283 12.941a.75.75 0 1 0 1.034 1.086zm6.434-4.056a.75.75 0 1 0-1.034-1.087zm-1.034-.001a.75.75 0 0 0 1.034-1.086zm-4.366-6.23a.75.75 0 1 0-1.034 1.087zM15.2 9.09a.75.75 0 0 0 0-1.5zM.8 7.59a.75.75 0 0 0 0 1.5zm9.517 6.438 5.4-5.143-1.034-1.087-5.4 5.143zm5.4-6.23-5.4-5.143L9.283 3.74l5.4 5.143zM15.2 7.59H.8v1.5h14.4z"
        fill="var(--cl-arrow)"
      />
    </svg>
  )
};

const BarSearch = ({ lang }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [isButtonActive, setIsButtonActive] = useState(false);
  const inputRef = useRef(null);

  const handleSearch = () => {
    const cleanedValue = searchTerm.trim(); // Eliminar espacios al inicio y final
    const words = cleanedValue.split(" ").filter((word) => word !== ""); // Contar palabras y eliminar espacios vacíos

    // Validar si hay entre 1 y 5 palabras
    if (words.length > 0 && words.length <= 5) {
      inputRef.current?.blur();
      handleSearchChange(cleanedValue);
    } else {
      console.log("Invalid bro");
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleSearch();
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setSearchTerm(value);
    setIsButtonActive(value.trim() !== "" && !value.includes(" "));
  };

  return (
    <div className={s.search_container}>
      <label htmlFor="search" className={s.input_search}>
        <IconSearch
          color="var(--color-contrast-700-t)"
          width={16}
          height={17}
        />
        <input
          ref={inputRef}
          className={s.input_typing}
          type="text"
          placeholder={lang}
          name="search"
          id="search"
          value={searchTerm}
          onChange={handleChange}
          onKeyPress={handleKeyPress}
        />
        <button
          className={`${s.btn_search} ${
            isButtonActive ? s.btn_search_activate : ""
          }`}
          onClick={handleSearch}
          disabled={!isButtonActive}
        >
          <IconArrowRight />
        </button>
      </label>
    </div>
  );
};

export default BarSearch;

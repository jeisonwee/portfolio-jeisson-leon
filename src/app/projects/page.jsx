/* -------------------------------- */
/* Pagina - Proyectos ------------- */
/* -------------------------------- */

import Image from "next/image";
import { langUser } from "@/scripts/langConfig.js";

// Componentes
import IcoTagBeta from "@/img/beta_tag.svg";
import ProjectsList from "./ProjectList";
import BarSearch from "./BarSearch";
import CategoriesList from "./CategoriesList";
import FooterMain from "@/components/FooterMain";

// Recursos
import BackgroundPattern from "@/img/background_projects.svg";
import IconCircle from "@/components/icons/IconCircle";
import IconProjects from "@/img/icons/icon_projects.svg";

// Estilos
import s from "./page.module.css";

// Categorias filtros y areas de interes
import categoriesIndex from "@/Enums/CategoriesTools.json";

export default function Projects() {
  //? Sistema de idiomas - Asi funciona la consulta del idioma
  const langProjects = langUser("projects");
  const { categories, titleCategories } = langProjects;

  return (
    <section className={s.page_container}>
      <article className={s.sec_1}>
        <div className={s.cont_circle_float}>
          <div className={s.sub_circle_float}>
            <IconCircle color="var(--color-50gray-200-t)" />
            <IconCircle color="var(--color-50gray-200-t)" />
          </div>
        </div>

        {/* Fondo de seccion */}
        <Image
          className={s.background_img}
          src={BackgroundPattern}
          alt="Background"
        />

        <div className={s.sec_1_info}>
          <span className={s.float_tag}>
            <Image src={IconProjects} alt="Icon" />
            <h1 className={s.tag_title}>{langProjects.title}</h1>
          </span>
          <h2 className={s.title_welcome}>{langProjects.description}</h2>

          <div className={s.cont_controls}>
            <BarSearch lang={langProjects.btnSearch} />
            <button className={s.btn_add_project}>
              {langProjects.btnAddPro} <Image src={IcoTagBeta} alt="Beta" />
            </button>
          </div>

          <CategoriesList
            titleCate={titleCategories}
            categories={categories}
            categoriesIndex={categoriesIndex}
          />
        </div>
      </article>

      <article className={s.sec_2}>
        <ProjectsList
          langCategories={categories}
          langTitle={langProjects.infoGuideProjects}
        />
      </article>

      <FooterMain />
    </section>
  );
}

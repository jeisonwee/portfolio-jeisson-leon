"use client";

import { useState } from "react";

// Estilos
import s from "./page.module.css";

// Recursos
import IconArrowLeft from "@/components/icons/IconArrowLeft";

const ElementSkill = ({ title, domination, description, subSkills }) => {
  const [deployState, setDeployState] = useState(false);

  return (
    <div className={s.cont_skill_main}>
      <section className={s.cont_head_skill}>
        <h4>{title}</h4>
        <section className={s.sub_cont_head_skill}>
          <span className={s.tx_domi}>{domination}%</span>
          <button
            className={s.btn_expand}
            onClick={() => setDeployState(!deployState)}
          >
            <IconArrowLeft
              width={8}
              height={10}
              color="var(--color-hov)"
              style={{
                transform: `${deployState ? "rotate(-90deg)" : "rotate(90deg)"}`,
              }}
            />
          </button>
        </section>
      </section>

      <div
        className={s.bar_domination}
        style={{ "--width-bar": `${domination}%` }}
      />
      {deployState && (
        <div className={s.cont_info_skill}>
          {description.map((item, index) => (
            <p className={s.descrip_skill} key={index}>
              {item}
            </p>
          ))}
          <ul className={s.cont_sub_skills}>
            {subSkills.map((item, index) => (
              <li key={index}>
                <b>{item.title}:</b> {item.description}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default ElementSkill;

"use client";

import { useState } from "react";
import Image from "next/image";

// Estilos
import s from "./page.module.css";

// Recursos
import PicProfileRayed from "@/img/pic_profile_rayed.png";
import PicProfile from "@/img/pic_profile.png";

function CardProfile() {
  const [hidden, setHidden] = useState(true);

  return (
    <div
      className={`${s.cardProfile} ${s.item}`}
      onMouseEnter={() => setHidden(false)}
      onMouseLeave={() => setHidden(true)}
    >
      {hidden ? (
        <Image
          className={s.pic_profile}
          src={PicProfileRayed}
          alt="Jeisson Leon"
        />
      ) : (
        <Image
          className={`${s.pic_profile} ${s.pic_hovered}`}
          src={PicProfile}
          alt="Jeisson Leon"
        />
      )}
    </div>
  );
}

export default CardProfile;

"use client";

import { useState, memo } from "react";
import ReactMarkdown from "react-markdown";

// Estilos
import s from "./page.module.css";

// Recursos
import Image from "next/image";
import IconArrowLeft from "@/components/icons/IconArrowLeft";

const CompMetodology = ({ metoCont }) => {
  const [stateDeploy, setStateDeploy] = useState(false);

  // Componente interprete de Markdown
  const MDCom = memo(({ content }) => {
    return <ReactMarkdown children={content} />;
  });

  return (
    <>
      <div
        className={s.cont_metodology}
        style={{
          "--display-after": `${stateDeploy ? "none" : "block"}`,
          height: `${stateDeploy ? "auto" : "20rem"}`,
        }}
      >
        <div className={s.container_text}>
          <h2 className={s.sub_subtitle}>{metoCont.title}</h2>
          <MDCom content={metoCont.body[0]} />

          <figure className={s.cont_info_img}>
            <Image
              src="/img/banner_metodology.png"
              alt="Banner diagram of metodology"
              title="Banner metodology of my work"
              width="600"
              height="192"
              loading="lazy"
            />
            <figcaption>{metoCont.figCap}</figcaption>
          </figure>
        </div>
        <div className={s.container_text}>
          {metoCont.bodyTwo.map((item) => {
            return <MDCom content={item} key={item} />;
          })}
        </div>
      </div>
      <div
        className={s.metodology_control}
        style={{ display: `${stateDeploy ? "none" : "grid"}` }}
      >
        <button
          className={s.btn_expand_metodology}
          onClick={() => setStateDeploy(!stateDeploy)}
        >
          {metoCont.extendInfo}
          <IconArrowLeft
            width={10}
            height={12}
            style={{ transform: "rotate(90deg)" }}
            color="var(--color-contrast-700-t)"
          />
        </button>
      </div>
    </>
  );
};

export default CompMetodology;

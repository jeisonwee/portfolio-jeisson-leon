/* -------------------------------- */
/* Pagina - Acerca de mi ---------- */
/* -------------------------------- */
import { langUser } from "@/scripts/langConfig.js";
import Image from "next/image";
import Link from "next/link";
import { memo } from "react";
import ReactMarkdown from "react-markdown";

// Estilos
import s from "./page.module.css";

// Componentes
import TempLastCommit from "@/components/HomeComps/TempLastCommit";
import TempLastPro from "@/components/HomeComps/TempLastPro";
import CardProfile from "./CardProfile";
import CardProject from "@/components/ListProject/CardProject";
import CompMetodology from "./CompMetodo";
import ElementSkill from "./ElementSkill";
import GoToWork from "@/components/GoToWork";
import FooterMain from "@/components/FooterMain";

// Recursos
import BackgroundPattern from "@/img/background_about.svg";
import BackEventium from "@/img/back_eventium.svg";
import IconAbout from "@/img/icons/icon_about.svg";
import IconBogota from "@/img/icons/icon_bogota.png";
import IconSepa from "@/components/icons/IconSepa.jsx";
import BannerCode from "@/img/banner_code.png";
import BackPatternNums from "@/img/back_pattern_nums.svg";
import IconClipboard from "@/img/icons/ico_clipboard.svg";
import DivElementRay from "@/img/div_element_ray.svg";

import ProjectIco from "@/components/icons/ProjectIco";
import IconStars from "@/components/icons/IconStars";
import IconArrowLeft from "@/components/icons/IconArrowLeft";

import IconSkillsTools from "@/components/icons/IconSkillsTools";

import IconWorkExperience from "@/components/icons/IconWorkExperience";

import IconEducation from "@/components/icons/IconEducation";

// Informacion
import { getProjectsFeatured } from "@/db/queries";

const ContentStart = async ({ api }) => {
  try {
    const response = await fetch(api);
    if (!response.ok) {
      throw new Error("Respuesta de la red no fue ok");
    }
    const repos = await response.json();

    return <TempLastPro content={repos} />;
  } catch (error) {
    console.error("Error al obtener datos de GitHub:", error);
    return <b style={{ color: "red" }}>{"Ocurrio un problema :("}</b>;
  }
};
const ScrollList = () => {
  return (
    <div className={s.marquee_content}>
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.ediGithub}</p>
        <ContentStart api="https://api.github.com/users/JeissonWeeDev/repos?sort=updated&per_page=1" />
      </span>
      <IconSepa color="var(--color-contrast-500-t)" />
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.ediGitlab}</p>
        <ContentStart api="https://gitlab.com/api/v4/users/jeisonwee/projects?order_by=last_activity_at&per_page=1" />
      </span>
      <IconSepa color="var(--color-contrast-500-t)" />

      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.lastCommGithub}</p>
        <TempLastCommit apiUrl="https://api.github.com/users/JeissonWeeDev/repos?sort=updated&per_page=1" />
      </span>
      <IconSepa color="var(--color-contrast-500-t)" />
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.lastCommGitlab}</p>
        <TempLastCommit apiUrl="https://gitlab.com/api/v4/users/jeisonwee/projects?order_by=last_activity_at&per_page=1" />
      </span>
    </div>
  );
};

// Estado de disponibilidad
const avaible = true;

// Informacion de contacto basica
const contactList = [
  {
    pic: "/icons/icon_linkedin.svg",
    link: "#",
    user: "@Jeisson León",
  },
  {
    pic: "/icons/icon_gmail.svg",
    link: "#",
    user: "jei.buss@gmail.com",
  },
  {
    pic: "/icons/icon_gmail.svg",
    link: "#",
    user: "jeissonwee.dev@gmail.com",
  },
];
const ElementContact = ({ user, pic, link }) => {
  return (
    <Link href={link} className={s.btn_commu}>
      <Image src={pic} width="18" height="16" alt="icon" />
      <p>{user}</p>
    </Link>
  );
};

// Informacion de tecnologias recientes
const techRecentList = [
  {
    title: "NextJs",
    image: "/icons/tools/ico_vercel.png",
  },
  {
    title: "SvelteKit",
    image: "/icons/tools/ico_svelte.png",
  },
  {
    title: "BunJs",
    image: "/icons/tools/ico_bun.png",
  },
  {
    title: "Python",
    image: "/icons/tools/ico_python.png",
  },
  {
    title: "Bash",
    image: "/icons/tools/ico_bash.png",
  },
  {
    title: "Rust",
    image: "/icons/tools/ico_rust.png",
  },
  {
    title: "Selenium",
    image: "/icons/tools/ico_selenium.png",
  },
  {
    title: "JavaScript (ES6+)",
    image: "/icons/tools/ico_javascript.png",
  },
  {
    title: "ReactJs",
    image: "/icons/tools/ico_react.png",
  },
  {
    title: "Golang",
    image: "/icons/tools/ico_golang.png",
  },
];
const ElementTool = ({ img, title }) => {
  return (
    <div className={s.ele_tool}>
      <Image src={img} width={16} height={16} alt="Icon" />
      <p>{title}</p>
    </div>
  );
};

// Lista de proyectos preferidos
const preferedProjects = async () => {
  1;
  // Mis proyectos que quiero resaltar
  const featuredProjectIds = [2, 1, 3, 4];

  try {
    const projects = await getProjectsFeatured(featuredProjectIds);
    return projects;
  } catch (error) {
    console.error("Error obteniendo proyectos:", error);
    return false;
  }
};
const projectsExtracted = await preferedProjects();

// --> Error de extraccion de proyectos
const ErrorBro = () => {
  return (
    <div className={s.cont_error_projects}>
      <p>Error: 500</p>
      <p>Error bro :/</p>
    </div>
  );
};

// Elemento para la representacion de cada habilidad y su informacion basicamente
const ElementTools = ({ title, tools }) => {
  return (
    <div className={s.cont_area_tools}>
      <h4>{title}:</h4>
      <ul className={s.list_tools}>
        {tools.map((item, index) => {
          return (
            <li key={index}>
              <Link href={item.link ? item.link : "#"} target="_blank">
                <img src={item.logo} alt="Icon" loading="lazy" />
                {item.title}
                <span
                  className={s.ele_domi_tool}
                  style={{ "--width-tool": `${item.domination}%` }}
                />
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default function About() {
  const about = langUser("about");
  const { infoAbout, arti_3, arti_4, arti_5, arti_6, arti_7, arti_8 } = about;

  const langCategories = langUser("projects").categories;

  const MDCom = memo(({ content }) => {
    return <ReactMarkdown children={content} />;
  });

  return (
    <section className={s.page_container}>
      <div className={s.cont_circle_float}>
        <Image src={BackEventium} alt="Back Element" />
      </div>
      <article className={s.sec_1}>
        
        {/* Fondo de seccion */}
        <Image
          className={s.background_img}
          src={BackgroundPattern}
          alt="Background"
        />

        <div className={s.sec_1_info}>
          <span className={s.float_tag}>
            <Image src={IconAbout} alt="Icon" />
            <h1 className={s.tag_title}>{about.title}</h1>
          </span>

          <h1 className={s.title_representation}>Jeisson León</h1>
          <h2 className={s.title_welcome}>{about.description}</h2>

          <div className={s.cont_tags}>
            <span className={s.making_bogota}>
              <p>{about.tagMaking}</p>
              <Image src={IconBogota} width={16} height={10.67} alt="Icon" />
            </span>

            <div className={s.tag_item}>
              <span>{about.tagServices}</span>
            </div>
          </div>
        </div>
      </article>

      <article className={s.sec_2}>
        <div className={`${s.elementScroll} ${s.item}`}>
          <div className={s.marquee}>
            <ScrollList />
          </div>
        </div>

        <div className={s.first_info}>
          <div className={s.info_first_1}>
            <CardProfile />
            <Image src={BannerCode} alt="Banner Code" />
          </div>

          <div className={s.info_first_2}>
            <div className={s.cont_1}>
              <div className={`${s.info_person} ${s.info_1}`}>
                <h3>{infoAbout.nameTitle}</h3>
                <p>Jeisson S. León Moreno</p>
              </div>
              <div className={`${s.info_person} ${s.info_2}`}>
                <h3>{infoAbout.experienceTitle}</h3>
                <p>{infoAbout.experience}</p>
              </div>
              <div className={`${s.info_person} ${s.info_3}`}>
                <h3>{infoAbout.availabilityTitle}</h3>
                <p className={s.disp_active}>
                  <span />
                  {avaible
                    ? infoAbout.availability.yes
                    : infoAbout.availability.no}
                </p>
              </div>
              <div className={`${s.info_person} ${s.info_4}`}>
                <h3>{infoAbout.basedTitle}</h3>
                <p>{infoAbout.based}</p>
              </div>
              <div className={`${s.info_person} ${s.info_5}`}>
                <h3>{infoAbout.langsKnowTitle}</h3>
                <p>{infoAbout.langsKnow}</p>
              </div>
            </div>
          </div>

          <div className={s.info_first_3}>
            <div className={s.cont_2}>
              {contactList.map((item) => {
                return (
                  <ElementContact
                    key={item.user}
                    user={item.user}
                    pic={item.pic}
                    link={item.link}
                  />
                );
              })}
            </div>

            <div className={s.cont_3}>
              <h3>{infoAbout.toolUsed}:</h3>
              <div className={s.cont_tools_recents}>
                {techRecentList.map((item) => {
                  return (
                    <ElementTool
                      key={item.title}
                      img={item.image}
                      title={item.title}
                    />
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </article>

      <article className={s.sec_3}>
        <div className={s.container_text}>
          <h2 className={s.subtitle}>{arti_3.title}</h2>
          {arti_3.body.map((text) => {
            return <MDCom content={text} key={text} />;
          })}

          <div className={s.cont_expo_info}>
            <div className={s.cont_nums}>
              <div className={s.numbers_cont}>
                <div>
                  <h4>+1200</h4>
                  <p>{arti_3.expoContent.first}</p>
                </div>
                <div>
                  <h4>+30</h4>
                  <p>{arti_3.expoContent.second}</p>
                </div>
              </div>
              <Image className={s.back_nums} src={BackPatternNums} alt="Back" />
            </div>

            <div className={s.cont_coding}>
              <div className={s.code_head}>
                <h4>Javascript</h4>
                <button className={s.btn_copy}>
                  <Image src={IconClipboard} alt="Clipboard" />
                </button>
              </div>
              <div className={s.code_body}>
                <p>
                  // Cat cat bro...! <br />
                  const catASCII = ` <br />
                  |\__/,| (`\ <br />
                  |_ _ |.--.) ) <br />
                  ( T ) / <br />
                  (((^_(((/(((_/ <br />
                  `;
                  <br />
                  <br />
                  console.log(catASCII);
                </p>
              </div>
            </div>
          </div>
        </div>
      </article>

      <Image className={s.div_sections} src={DivElementRay} alt="Div" />

      <article className={s.sec_4}>
        <div className={s.sec_4_1}>
          <div className={s.container_text}>
            <h2 className={`${s.subtitle} ${s.escept_subtitle}`}>
              <ProjectIco color="var(--color-contrast-900-t)" />
              {arti_4.title}
            </h2>

            <MDCom content={arti_4.body[0]} />

            <figure className={s.cont_info_img}>
              <Image
                src="/img/banner_lionteam_brand.png"
                alt="Banner Lion Team"
                title="Banner Lion Team Org branding"
                width="450"
                height="150"
                loading="lazy"
              />
              <figcaption>{arti_4.figCap}</figcaption>
            </figure>
          </div>
        </div>
        <div className={s.container_text}>
          <MDCom content={arti_4.body[1]} />
        </div>

        <div className={s.cont_projects}>
          {projectsExtracted ? (
            <ul className={s.cont_projects_list}>
              {projectsExtracted.map((project, index) => (
                <CardProject
                  info={project}
                  langCategories={langCategories}
                  key={index}
                />
              ))}
            </ul>
          ) : (
            <ErrorBro />
          )}

          <div className={s.cont_more_projects}>
            <p>
              <IconStars color={"var(--color-contrast-700-t)"} />
              {arti_4.moreProjects}
            </p>
            <Link href="/projects" className={s.btn_projects}>
              {arti_4.btnProjects}
              <IconArrowLeft
                width={10}
                height={12}
                style={{ transform: "rotate(180deg)" }}
                color="var(--color-contrast-700-t)"
              />
            </Link>
          </div>
        </div>

        <span className={s.div_sub_sections} />

        <CompMetodology metoCont={arti_4.metoCont} />
      </article>

      <Image className={s.div_sections} src={DivElementRay} alt="Div" />

      <article className={s.sec_5}>
        <div className={s.container_text}>
          <h2 className={`${s.subtitle} ${s.escept_subtitle}`}>
            <IconSkillsTools color="var(--color-contrast-900)" />
            {arti_5.title}
          </h2>
          <h5 className={s.slogan_title}>{arti_5.description}</h5>
          <div className={`${s.box_text} ${s.container_text}`}>
            {arti_5.body.map((text) => (
              <MDCom content={text} key={text} />
            ))}
          </div>
        </div>

        <div className={s.cont_skills}>
          <div className={s.head_container}>
            <h3 className={s.sub_subtitle_head}>{arti_5.skills.title}</h3>
          </div>

          <div className={s.cont_list_skill}>
            {arti_5.skills.listSkills.map((item) => {
              return (
                <ElementSkill
                  key={item.title}
                  title={item.title}
                  domination={item.domination}
                  description={item.description}
                  subSkills={item.subSkills}
                />
              );
            })}
          </div>
        </div>

        <div className={s.cont_tools}>
          <div className={s.head_container}>
            <h3 className={s.sub_subtitle_head}>{arti_5.tools.title}</h3>
          </div>

          <div className={s.cont_list_tools}>
            {arti_5.tools.listTools.map((item, index) => {
              return (
                <ElementTools
                  key={index}
                  title={item.title}
                  tools={item.tools}
                />
              );
            })}
          </div>
        </div>
      </article>

      <Image className={s.div_sections} src={DivElementRay} alt="Div" />

      <article className={s.sec_6}>
        <div className={s.container_text}>
          <h2 className={`${s.subtitle} ${s.escept_subtitle}`}>
            <IconWorkExperience color="var(--color-contrast-900)" />
            {arti_6.title}
            <div className={s.tag_sub_title}>
              <span>{arti_6.sinceYears}</span>
              <span>{arti_6.tagYears}</span>
            </div>
          </h2>
          <h5 className={s.slogan_title}>{arti_6.description}</h5>

          <div className={`${s.box_text} ${s.container_text}`}>
            <MDCom content={arti_6.body[0]} />
          </div>
        </div>

        <div className={s.sec_6_cont_experience}>
          {arti_6.experienceList.map((item, index) => {
            return (
              <div className={s.info_experience} key={index}>
                <div className={s.exp_guide}>
                  <div className={s.img_logo}>
                    <Image src={item.logo} width="32" height="32" alt="Logo" />
                  </div>

                  <div className={s.figure_info}>
                    <span className={s.circle_guide} />
                    <span className={s.ray_guide} />
                  </div>
                </div>

                {item.type === 1 && (
                  <div className={s.info_exp_cont}>
                    <section className={s.exp_head}>
                      <h3>{item.title}</h3>
                      <p>{item.info}</p>
                    </section>

                    <section className={s.exp_descri}>
                      {item.description.map((item, index) => {
                        return <p key={index}>{item}</p>;
                      })}
                    </section>

                    <section className={s.cont_list_exp}>
                      <h4>Mis Responsabilidades:</h4>

                      <ul className={s.cont_sub_skills}>
                        {item.responsabilities.map((item, index) => (
                          <li key={index} className={s.li_exp}>
                            {item}
                          </li>
                        ))}
                      </ul>
                    </section>

                    <section className={s.cont_list_exp}>
                      <h4>Mis Logros:</h4>

                      <ul className={s.cont_sub_skills}>
                        {item.achievements.map((item, index) => (
                          <li key={index} className={s.li_exp}>
                            {item}
                          </li>
                        ))}
                      </ul>
                    </section>

                    <section className={s.cont_list_skills}>
                      {item.skills.map((item, index) => {
                        return (
                          <span key={index} className={s.ele_skill}>
                            {item}
                          </span>
                        );
                      })}
                    </section>
                  </div>
                )}

                {item.type === 2 && (
                  <div className={s.sub_info_exp_list}>
                    <div className={s.info_exp_cont}>
                      <section className={s.exp_head}>
                        <h3>{item.title}</h3>
                        <p>{item.info}</p>
                      </section>
                    </div>

                    {item.roles.map((item, index) => {
                      return (
                        <div key={index} className={s.cont_sub_element_exp}>
                          <div className={s.figure_info_sub}>
                            <span className={s.circle_guide_sub} />
                            <span className={s.ray_guide_sub} />
                          </div>

                          <div
                            key={index}
                            className={s.info_exp_cont}
                            style={{ padding: "0 0 0 0" }}
                          >
                            <section className={s.exp_head}>
                              <h3>{item.title}</h3>
                              <p>{item.info}</p>
                            </section>

                            <section className={s.exp_descri}>
                              {item.description.map((item, index) => {
                                return <p key={index}>{item}</p>;
                              })}
                            </section>

                            <section className={s.cont_list_exp}>
                              <h4>Mis Responsabilidades:</h4>
                              <ul className={s.cont_sub_skills}>
                                {item.responsabilities.map((item, index) => (
                                  <li key={index} className={s.li_exp}>
                                    {item}
                                  </li>
                                ))}
                              </ul>
                            </section>

                            <section className={s.cont_list_exp}>
                              <h4>Mis Logros:</h4>
                              <ul className={s.cont_sub_skills}>
                                {item.achievements.map((item, index) => (
                                  <li key={index} className={s.li_exp}>
                                    {item}
                                  </li>
                                ))}
                              </ul>
                            </section>

                            <section className={s.cont_list_skills}>
                              {item.skills.map((item, index) => {
                                return (
                                  <span key={index} className={s.ele_skill}>
                                    {item}
                                  </span>
                                );
                              })}
                            </section>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            );
          })}
        </div>
      </article>

      <Image className={s.div_sections} src={DivElementRay} alt="Div" />

      <article className={s.sec_7}>
        <div className={s.container_text}>
          <h2 className={`${s.subtitle} ${s.escept_subtitle}`}>
            <IconEducation color="var(--color-contrast-900)" />
            {arti_7.title}
          </h2>

          <div className={`${s.box_text} ${s.container_text}`}>
            <MDCom content={arti_7.body[0]} />
          </div>

          <div className={s.cont_education}>
            {arti_7.educationList.map((item, index) => {
              return (
                <div className={s.info_experience} key={index}>
                  <div className={s.exp_guide}>
                    <div className={s.img_logo}>
                      <Image
                        src={item.logo}
                        width="32"
                        height="32"
                        alt="Logo"
                      />
                    </div>

                    <div className={s.figure_info}>
                      <span className={s.circle_guide} />
                      <span className={s.ray_guide} />
                    </div>
                  </div>

                  <div className={`${s.info_exp_cont} ${s.info_edu_cont}`}>
                    <section className={s.exp_head}>
                      <h3>{item.title}</h3>
                      {item.info.map((item, index) => {
                        return <p key={index}>{item}</p>;
                      })}
                    </section>
                  </div>
                </div>
              );
            })}
          </div>

          <div className={s.cont_certifications}>
            <div className={s.head_container}>
              <h3 className={s.sub_subtitle_head}>{arti_8.title}</h3>
            </div>

            <div className={s.cont_list_certifications}>
              {arti_8.certificationList.map((item, index) => {
                return (
                  <div className={s.info_experience} key={index}>
                    <div className={s.exp_guide}>
                      <div className={s.img_logo}>
                        <Image
                          src={item.logo}
                          width="32"
                          height="32"
                          alt="Logo"
                        />
                      </div>
                    </div>

                    <div className={`${s.info_exp_cont} ${s.info_edu_cont}`}>
                      <section className={s.exp_head}>
                        <h3>{item.title}</h3>
                        {item.info.map((item, index) => {
                          return <p key={index}>{item}</p>;
                        })}
                      </section>

                      <div className={s.cont_list_skills}>
                        {item.skills.map((item, index) => {
                          return (
                            <span key={index} className={s.ele_skill}>
                              {item}
                            </span>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </article>

      <Image className={s.div_sections} src={DivElementRay} alt="Div" />

      <GoToWork />
      <FooterMain />
    </section>
  );
}

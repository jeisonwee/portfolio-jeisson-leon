import { langUserPre, langUser } from "@/scripts/langConfig";

// Components
import LayoutMain from "@/components/LayoutMain";
import ThemeControl from "@/components/ThemeControl";
import KeepInDev from "@/components/Advises/KeepInDev";

// Styles
import "./globals.css";

export const metadata = {
  title: "Jeisson Leon",
  description: "Portafolio de Jeisson Leon",
};

export default function RootLayout({ children }) {
  const lang = langUserPre();

  //? Informacion del modal de aviso
  const AdviseInfo = langUser("advises").keepInDev;

  return (
    <html lang={lang}>
      <body>
        <ThemeControl>
          <KeepInDev AdviseInfo={AdviseInfo} />
          <LayoutMain>{children}</LayoutMain>
        </ThemeControl>
      </body>
    </html>
  );
}

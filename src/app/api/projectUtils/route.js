import { NextResponse } from "next/server";
import {
  getProjects,
  getProjectsByTag,
  getProjectsBySearch,
} from "@/db/queries";

//? Codigo de pruebas no descomentar ------------
/* 
  function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
  await delay(2000);
*/

// Metodo para la consulta controlada de bloques
export async function GET(request) {
  const { searchParams } = new URL(request.url);

  //* Metodo interno - Consulta de proyectos por bloques de forma sencilla
  if (searchParams.get("page")) {
    const page = searchParams.get("page")
      ? parseInt(searchParams.get("page"))
      : 1;

    const limit = 6; // Limite fijo de registros predefinido

    try {
      const projects = await getProjects(page, limit);

      // Verificar si hay registros en la página solicitada
      if (projects.length === 0) {
        return NextResponse.json({ error: "No more records" }, { status: 404 });
      }

      return NextResponse.json({ projects }, { status: 200 });
    } catch (error) {
      console.error("Error during processing:", error);
      return NextResponse.json(
        { error: "Failed to retrieve projects" },
        { status: 500 }
      );
    }
  }

  //* Metodo interno - Consulta de proyectos por tag/categoria
  if (searchParams.get("tag")) {
    const tag = searchParams.get("tag");
    const block = searchParams.get("block")
      ? parseInt(searchParams.get("block"), 10)
      : 1;

    const limit = 6;

    // Validación del parámetro block
    if (!searchParams.get("block")) {
      return NextResponse.json(
        { error: "Missing block parameter" },
        { status: 400 }
      );
    }
    if (isNaN(block) || block < 1) {
      return NextResponse.json(
        { error: "Invalid block parameter. Block must be a positive integer." },
        { status: 400 }
      );
    }

    if (!searchParams.get("tag")) {
      return NextResponse.json(
        { error: "Missing tag parameter" },
        { status: 400 }
      );
    }

    if (tag.trim() === "" || typeof tag !== "string") {
      return NextResponse.json(
        { error: "Invalid tag parameter. Tag must be a non-empty string." },
        { status: 400 }
      );
    }

    try {
      const projects = await getProjectsByTag(tag, block, limit);

      if (projects.length === 0) {
        return NextResponse.json({ error: "Tag not found" }, { status: 404 });
      }

      return NextResponse.json({ projects }, { status: 200 });
    } catch (error) {
      console.error("Error fetching projects by tag:", error);

      if (error.code === "42804" || error.code === "42601") {
        // Errores de sintaxis o tipo de dato
        return NextResponse.json(
          { error: "Invalid query parameters" },
          { status: 400 }
        );
      } else {
        return NextResponse.json({ error: "Tag not found" }, { status: 500 });
      }
    }
  }

  //* Metodo interno - Consulta de proyectos termino de busqueda
  if (searchParams.get("search")) {
    const block = searchParams.get("block")
      ? parseInt(searchParams.get("block"), 10)
      : 1;

    const limit = 6; // Limite fijo de registros predefinido
    const search = searchParams.get("search") || "";

    // Validación del parámetro block
    if (!searchParams.get("block")) {
      return NextResponse.json(
        { error: "Missing block parameter" },
        { status: 400 }
      );
    }
    if (isNaN(block) || block < 1) {
      return NextResponse.json(
        { error: "Invalid block parameter. Block must be a positive integer." },
        { status: 400 }
      );
    }

    try {
      let projects = await getProjectsBySearch(block, limit, search);

      // Verificar si hay registros en la página solicitada
      if (projects.length === 0) {
        return NextResponse.json({ error: "No more records" }, { status: 404 });
      }

      return NextResponse.json({ projects }, { status: 200 });
    } catch (error) {
      console.error("Error during processing:", error);
      return NextResponse.json(
        { error: "Failed to retrieve projects" },
        { status: 500 }
      );
    }
  }

  return NextResponse.json(
    { error: "Que estas haciendo perra !?" },
    { status: 500 }
  );
}

export async function POST(req) {
  const res = NextResponse;

  try {
    // Envía la respuesta con el email anonimizado
    return res.json({ e: "Sexo anal en post" }, { status: 200 });
  } catch (error) {
    console.error("Error during processing:", error); // Depuración: error durante el procesamiento
    return res.json({ error: "Failed to send email" }, { status: 500 });
  }

  return res.json({ mess: "Vos que haces loka?!" }, { status: 500 });
}

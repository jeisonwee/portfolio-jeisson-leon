/* ---- Metodo de cambio de idioma con el uso de cookies ---- */

import { NextResponse } from "next/server";

const validLocales = ["es", "en", "fr"];

export async function GET(request) {
  const { searchParams } = new URL(request.url);
  const newValue = searchParams.get("value");

  if (!newValue) {
    return NextResponse.json({ error: "Invalid method" }, { status: 400 });
  }

  // Verificar si el nuevo valor es uno de los permitidos
  if (!validLocales.includes(newValue)) {
    return NextResponse.json({ error: "Invalid value" }, { status: 400 });
  }

  // Configurar la cookie con el nuevo valor
  const response = NextResponse.json({ msg: "Cookie updated", newValue });
  response.cookies.set("preferredLocale", newValue, { path: "/" });

  return response;
}

/* ---- Metodo de descarga de Resumen---- */

import { NextResponse } from "next/server";

export async function GET(req) {
  const res = NextResponse;
  const searchParams = req.nextUrl.searchParams;
  const type = searchParams.get("type");
  const lang = searchParams.get("lang");

  //!Esta porqueria tiene que ser migrada a variables de entorno para actualizaciones de documentacion y experiencia en relacion
  const FILE_IDS = {
    pdf: {
      en: "PDF_FILE_ID_ENGLISH",
      es: "PDF_FILE_ID_SPANISH",
    },
    word: {
      en: "WORD_FILE_ID_ENGLISH",
      es: "WORD_FILE_ID_SPANISH",
    },
  };

  try {
    // Verificar que los parámetros sean válidos
    if (!["pdf", "word"].includes(type) || !["en", "es"].includes(lang)) {
      return res.json({ error: "Invalid file type or language" }, { status: 400 });
    }

    const fileId = FILE_IDS[type][lang];

    const downloadUrl = `https://drive.google.com/uc?export=download&id=${fileId}`;

    return res.json({ downloadUrl }, { status: 200 });
  } catch (error) {
    return res.json({ error: "Failed to generate download link" }, { status: 500 });
  }

  return res.json({ mess: "What u fucking doing?!" }, { status: 500 });
}

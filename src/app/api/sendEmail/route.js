/* ---- Metodo para recibir el email del remitente quien recibe el Resumen ---- */

import { NextResponse } from "next/server";
import nodemailer from "nodemailer";

import dotenv from "dotenv";
dotenv.config();

// Cargar variables de entorno
const { SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASS } = process.env;

//!Esta porqueria tiene que ser migrada a variables de entorno para actualizaciones de documentacion y experiencia en relacion
// Identificador de tipado e idioma del documento preferido
const file_ids = {
  pdf: {
    en: "1qFJqGwzz_0XptFaBo7Wd_TxYSRXH1-uM",
    es: "1smrbp-zW2k1fd-I4GuP-BLiJpJYm7m8O",
  },
  word: {
    en: "1yvu9TvjlF7jcz0c6fHus25t3eEWQ8ZWc",
    es: "1j-K42L4HKJb6ljngNTtQTVMlc1582VsU",
  },
};

export async function POST(req) {
  const res = NextResponse;

  const { email, lang, type } = await req.json();

  // Validación de parámetros
  if (!email) {
    return res.status(400).json({ error: "Email is required" });
  }

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(email)) {
    return res.status(400).json({ error: "Invalid email format" });
  }

  if (!lang) {
    return res.status(400).json({ error: "Language is required" });
  }

  if (!["en", "es"].includes(lang)) {
    return res
      .status(400)
      .json({ error: 'Invalid language, must be "en" or "es"' });
  }

  try {
    // Verifica los valores en el identificador de tipo y lenguaje
    if (!["pdf", "word"].includes(type) || !["en", "es"].includes(lang)) {
      return res.status(400).json({ error: "Invalid file type or language" });
    }

    const fileId = file_ids[type][lang];
    const downloadUrl = `https://drive.google.com/uc?export=download&id=${fileId}`;

    // Descargar el archivo desde Google Drive
    const response = await fetch(downloadUrl);
    if (!response.ok) {
      throw new Error("!!!!Failed to download file");
    }

    const arrayBuffer = await response.arrayBuffer();
    const buffer = Buffer.from(arrayBuffer);

    //! El contenido de las traducciones debe tomarse del sistema de lenguaje ya diseñado con "LangUserConfig.js" o en su defecto tomar la cookie entrante para realizar el envio necesario prbo.
    const emailSubject =
      lang === "en"
        ? "Your Requested Information"
        : "La Información Solicitada";
    const emailBody =
      lang === "en"
        ? `<div>
           <p>Hello,</p>
           <p>Here is the information you requested.</p>
           <p>Best regards,</p>
           <p>Your Company</p>
         </div>`
        : `<div>
           <p>Hola,</p>
           <p>Aquí está la información que solicitaste.</p>
           <p>Saludos,</p>
           <p>Tu Compañía</p>
         </div>`;

    // Configurar Nodemailer con variables de entorno
    const transporter = nodemailer.createTransport({
      host: SMTP_HOST,
      port: SMTP_PORT,
      secure: true, // true para 465, false para otros puertos
      auth: {
        user: SMTP_USER,
        pass: SMTP_PASS,
      },
    });

    const mailOptions = {
      from: `"Jeisson Leon" <${SMTP_USER}>`, // Remitente
      to: email, // Destinatario
      subject: emailSubject, // Asunto
      html: emailBody, // Cuerpo en HTML
      attachments: [
        {
          filename: `JeiLeonResume_${lang}.${type === "pdf" ? "pdf" : "docx"}`, // Nombre del archivo adjunto
          content: buffer, // Contenido del archivo
        },
      ],
    };

    // Enviar el correo
    await transporter.sendMail(mailOptions);

    // Máscara del email dejando visibles los primeros 2 o 3 caracteres
    const maskedEmail = email.replace(/(\w{2,3})[\w.-]*@/, "$1*****@");

    // Envía la respuesta con el email anonimizado
    return res.json({ e: maskedEmail }, { status: 200 });
  } catch (error) {
    console.error("Error during processing:", error); // Depuración: error durante el procesamiento
    return res.json({ error: "Failed to send email" }, { status: 500 });
  }

  return res.json({ mess: "Vos que haces loka?!" }, { status: 500 });
}

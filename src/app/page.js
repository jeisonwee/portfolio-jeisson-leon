import Image from "next/image";
import { langUser } from "@/scripts/langConfig.js";

// Componentes
import Card from "@/components/HomeComps/Card";
import CardLink from "@/components/HomeComps/CardLink";
import TempLastPro from "@/components/HomeComps/TempLastPro";
import TempLastCommit from "@/components/HomeComps/TempLastCommit";
import ContNet from "@/components/HomeComps/ContNet";
import GoToWork from "@/components/GoToWork";
import FooterMain from "@/components/FooterMain";

// Estilos
import s from "./page.module.css";

// Recursos
import Iconref from "@/img/icon_refering.svg";
import IconCreative from "@/components/icons/IconCreativeInnovation";
import BackImgCover from "@/img/back_image_port.webp";
import PicProfile from "@/img/pic_profile.png";
import IconSepa from "@/components/icons/IconSepa.jsx";

import BackAbout from "@/components/icons/BackAboutCard";
import IconAbout from "@/img/icons/icon_about.svg";
import IconArrowR from "@/components/icons/IconArrowR";

import BackProjects from "@/img/back_projects.png";
import IconProjects from "@/img/icons/icon_projects.svg";
import IconLionT from "@/components/icons/IconLionTeamVer";
import IconCircle from "@/components/icons/IconCircle";

import BackServices from "@/img/back_services.png";
import IconServices from "@/img/icons/icon_services.svg";

import RefContact from "@/components/icons/ReferencesPic";

import BackBlog from "@/img/back_blog_dark.png";
import IconBlog from "@/img/icons/icon_blog.svg";
import IconLang from "@/components/icons/IconLangs";
import FigCom from "@/components/HomeComps/FigCom";
import FigCom2 from "@/components/HomeComps/FigCom2";

import SliderImages from "@/components/HomeComps/SliderImages";

import AreaSkill from "@/components/HomeComps/AreaSkils";

const ContentStart = async ({ api }) => {
  try {
    const response = await fetch(api);
    if (!response.ok) {
      throw new Error("Respuesta de la red no fue ok");
    }
    const repos = await response.json();

    return <TempLastPro content={repos} />;
  } catch (error) {
    console.error("Error al obtener datos de GitHub:", error);
    return <b style={{ color: "red" }}>{":("}</b>;
  }
};

const ScrollList = () => {
  return (
    <div className={s.marquee_content}>
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.ediGitlab}</p>
        <ContentStart api="https://gitlab.com/api/v4/users/jeisonwee/projects?order_by=last_activity_at&per_page=1" />
      </span>
      <IconSepa color="var(--color-contrast-500-t)" />
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.lastCommGitlab}</p>
        <TempLastCommit apiUrl="https://gitlab.com/api/v4/users/jeisonwee/projects?order_by=last_activity_at&per_page=1" />
      </span>
      <IconSepa color="var(--color-contrast-500-t)" />
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.ediGithub}</p>
        <ContentStart api="https://api.github.com/users/JeissonWeeDev/repos?sort=updated&per_page=1" />
      </span>
      <IconSepa color="var(--color-contrast-500-t)" />
      <span className={s.item_marquee}>
        <p>{langUser("home").sectionCard.carrusell.lastCommGithub}</p>
        <TempLastCommit apiUrl="https://api.github.com/users/JeissonWeeDev/repos?sort=updated&per_page=1" />
      </span>
    </div>
  );
};

// Informacion de imagenes para presentar
const listImages = [
  "/img/Pinchefoto.png",
  "/img/nyan_cat.png",
  "/img/compu_mini.png",
  "/img/compu_bro.png",
  "/img/jei_maniya.png",
  "/img/my_compu.png",
  "/img/bannerbro.png",
  "/img/jei_karen.jpg",
]
// Informacion de contacto
const listContact = [
  {
    username: "@Jeisson Leon",
    linkRef: "https://www.linkedin.com/in/jeisson-le%C3%B3n-53b05a1a8/",
    picRef:
      "https://gitlab.com/uploads/-/system/user/avatar/10955695/avatar.png?width=100",
    logoRef: "/icons/icon_linkedin.svg",
  },
  {
    username: "@jeissonwee",
    linkRef: "https://www.instagram.com/jeissonwee/",
    picRef: "/img/pic_profile_instagram.jpg",
    logoRef: "/icons/icon_instagram.svg",
  },
  {
    username: "Jeissonwee.dev@gmail.com",
    subtx: "Desarrollo",
    linkRef: "mailto:jeissonwee.dev@gmail.com",
    picRef:
      "https://lh3.googleusercontent.com/a/ACg8ocIiE9fFUjvC1PNq3Ed4rrh9i7wL-ZQ_0A6ohASooupGFB-UhuY=s275-c-no",
    logoRef: "/icons/icon_gmail.svg",
  },
  {
    username: "Jei.buss@gmail.com",
    subtx: "Negocios",
    linkRef: "mailto:jei.buss@gmail.com",
    picRef:
      "https://lh3.googleusercontent.com/a/ACg8ocIiE9fFUjvC1PNq3Ed4rrh9i7wL-ZQ_0A6ohASooupGFB-UhuY=s275-c-no",
    logoRef: "/icons/icon_gmail.svg",
  },
  {
    username: "@jeissonwee.dev",
    subtx: "Desarrollo",
    linkRef: "https://gitlab.com/jeissonwee.dev",
    picRef:
      "https://gitlab.com/uploads/-/system/user/avatar/14092376/avatar.png?width=100",
    logoRef: "/icons/icon_gitlab.svg",
  },
  {
    username: "@jeisonwee",
    subtx: "Personal",
    linkRef: "https://gitlab.com/jeisonwee",
    picRef:
      "https://gitlab.com/uploads/-/system/user/avatar/10955695/avatar.png?width=100",
    logoRef: "/icons/icon_gitlab.svg",
  },
  {
    username: "@JeissonWeeDev",
    linkRef: "https://github.com/JeissonWeeDev",
    picRef: "https://avatars.githubusercontent.com/u/133667664?v=4",
    logoRef: "/icons/icon_github.svg",
  },
];

export default async function Home() {
  const allLang = langUser("home");

  return (
    <section className={s.page_container}>
      <Image
        className={s.backImgCover}
        src={BackImgCover}
        alt="Cover background"
        width="200"
        height="fill"
      />

      <article className={s.page_sec_1}>
        <div className={s.cont_ref}>
          <Image src={Iconref} alt="Japan Sushi" width="fill" height="fill" />
        </div>

        <section className={s.cont_info}>
          <div className={s.tag_container}>
            <div className={s.tag_item}>
              <span>{langUser("home").tag}</span>
            </div>
            <IconCreative
              className={s.icon_creative}
              colorStroke="var(--color-contrast-200-t)"
              colorText="var(--color-contrast-900)"
            />
          </div>

          <div className={s.info_container}>
            <h3>{langUser("home").title}</h3>
            <h1>Jeisson León</h1>
            <h2>{langUser("home").subtitle}</h2>
          </div>
        </section>
      </article>

      <article className={s.page_sec_2}>
        <div className={s.grid_container}>
          <Card className={s.cardPic} isTwoRow={true}>
            <Image
              className={s.pic_profile}
              src={PicProfile}
              alt="Jeisson Leon"
            />
          </Card>

          <Card className={s.elementScroll} isTwoCol={true}>
            <div className={s.marquee}>
              <ScrollList />
            </div>
          </Card>

          <CardLink link="/about" className={s.cardAbout}>
            <BackAbout className={s.pic_about} />

            <section className={s.about_float}>
              <Image src={IconAbout} alt="About" />

              <article className={s.about_info_cont}>
                <div className={s.about_info}>
                  <h4>{langUser("home").sectionCard.about.title}</h4>
                  <h2>{langUser("home").sectionCard.about.ref}</h2>
                </div>
                <IconArrowR clase={s.icon_arrow} color="var(--cl-arrow)" />
              </article>
            </section>
          </CardLink>

          <CardLink link="/projects" className={s.cardProjects}>
            <Image
              className={s.pic_projects}
              src={BackProjects}
              width="150"
              height="150"
              alt="Background Project"
            />
            <section className={s.projects_float}>
              <Image src={IconProjects} alt="Projects" />

              <IconLionT color="red" />

              <IconCircle color="var(--color-contrast-700-t)" />

              <article className={s.projects_info_cont}>
                <div className={s.projects_info}>
                  <h4>{langUser("home").sectionCard.projects.title}</h4>
                  <h2>{langUser("home").sectionCard.projects.ref}</h2>
                </div>
                <IconArrowR clase={s.icon_arrow} color="var(--cl-arrow)" />
              </article>
            </section>
          </CardLink>

          <CardLink link="/services" className={s.cardSer} isTwoCol={true}>
            <Image
              className={s.pic_services}
              src={BackServices}
              width="fill"
              alt="Background Services"
            />
            <section className={s.services_float}>
              <Image src={IconServices} alt="Services" />

              <article className={s.services_info_cont}>
                <div className={s.services_info}>
                  <h4>{langUser("home").sectionCard.services.title}</h4>
                  <h2>{langUser("home").sectionCard.services.ref}</h2>
                </div>
                <IconArrowR clase={s.icon_arrow} color="var(--cl-arrow)" />
              </article>
            </section>
          </CardLink>

          <Card className={s.cardInfo}>
            <article className={s.sub_card_info}>
              <h3>Full Developer</h3>
              <h2>Since 2018</h2>
            </article>
            <article className={s.sub_card_info_2}>
              <div className={s.info_2_cont}>
                <RefContact color="var(--color-contrast-60)" />
                <p>+6</p>
              </div>
            </article>
          </Card>

          <CardLink link="/blog" className={s.cardBlog}>
            <Image
              className={s.pic_blog}
              src={BackBlog}
              width="150"
              height="150"
              alt="Background Blog"
            />
            <section className={s.blog_head}>
              <IconLang
                className={s.icon_lang}
                color="var(--color-contrast-900)"
              />
              <Image
                className={s.icon_blog}
                src={IconBlog}
                width="fill"
                height={16.5}
                alt="Blog"
              />
            </section>
            <section className={s.blog_figures}>
              <FigCom clase={s.fig_blog_1} />
            </section>
            <section className={s.blog_figures_2}>
              <FigCom2 className={s.fig_blog_2} />
            </section>

            <section className={s.blog_info_cont}>
              <div className={s.blog_info}>
                <h4>{langUser("home").sectionCard.blog.title}</h4>
                <h2>{langUser("home").sectionCard.blog.ref}</h2>
              </div>
              <IconArrowR clase={s.icon_arrow} color="var(--cl-arrow)" />
            </section>
          </CardLink>

          <Card className={s.cardCarrusell} isTwoCol={true}>
            <SliderImages images={listImages} />
          </Card>
        </div>

        <article className={s.page_sec_3}>
          <p className={s.tx_home}>{allLang.txHome.fi}</p>

          <div className={s.list_contacts}>
            {listContact.map((contact, index) => (
              <ContNet
                key={index}
                username={contact.username}
                linkRef={contact.linkRef}
                picRef={contact.picRef}
                logoRef={contact.logoRef}
                subtx={contact.subtx}
              />
            ))}
          </div>
          <p className={s.tx_home}>{allLang.txHome.se}</p>
        </article>
      </article>

      <span className={s.div_sections} />

      {/* Seccion de "Things i can create" */}
      <AreaSkill langInfo={allLang.AreaSkills} />

      <span className={s.div_sections} />

      <GoToWork />
      <FooterMain />
    </section>
  );
}

"use client";

import s from "./test.module.css";

// Recursos
import { useTheme } from "@/components/ThemeControl";

export default function TestPage() {
  const theme = useTheme();

  return (
    <div>
      The curr theme is {theme}
      <p>Encontraste un Easter Egg bro 🥚</p>
    </div>
  );
}

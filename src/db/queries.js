import { pool } from "./index";

// Extrae los proyectos con paginación
async function getProjects(page = 1, limit = 1) {
  const offset = (page - 1) * limit; // Calcula desde qué registro empezar

  try {
    const query = `
      SELECT 
        projects.id, 
        projects.title, 
        projects.description, 
        projects.image, 
        projects.logo,
        projects.collaboration, 
        projects.tags, 
        projects.created_at, 
        projects.updated_at,
        users.full_name AS author, 
        users.profile_image AS profileImage
      FROM projects
      JOIN users ON projects.author_id = users.id
      ORDER BY projects.created_at DESC
      OFFSET $2
      LIMIT $1
    `;

    const result = await pool.query(query, [limit, offset]);
    return result.rows;
  } catch (err) {
    console.error("Error fetching projects:", err);
    throw err;
  }
}

async function getProjectsByTag(tag, block = 1, limit = 1) {
  const offset = (block - 1) * limit; // Calcula desde qué registro empezar
  try {
    const query = `
      SELECT
        projects.id, 
        projects.title, 
        projects.description, 
        projects.image, 
        projects.logo,
        projects.collaboration, 
        projects.tags, 
        projects.created_at, 
        projects.updated_at,
        users.full_name AS author, 
        users.profile_image AS profileImage
        FROM projects
        JOIN users ON projects.author_id = users.id
        WHERE projects.tags @> ARRAY[$1]
        ORDER BY projects.created_at DESC
        OFFSET $2
        LIMIT $3
    `;

    const result = await pool.query(query, [tag, offset, limit]);
    return result.rows;
  } catch (err) {
    console.error("Error fetching projects by tag:", err);
    throw err;
  }
}

async function getProjectsBySearch(page = 1, limit = 1, search = "") {
  const offset = (page - 1) * limit; // Calcula desde qué registro empezar

  // Validación y limpieza del input (opcional, pero buena práctica)
  const cleanedSearch = search.trim().replace(/[^a-zA-Z0-9\s]/g, "");

  try {
    const query = `
      SELECT 
        projects.id, 
        projects.title, 
        projects.description, 
        projects.image, 
        projects.logo,
        projects.collaboration, 
        projects.tags,
        users.full_name AS author, 
        users.profile_image AS profileImage
      FROM projects
      JOIN users ON projects.author_id = users.id
      WHERE LOWER(projects.title) LIKE LOWER($3) OR LOWER(projects.description) LIKE LOWER($3)
      ORDER BY projects.created_at DESC
      OFFSET $2
      LIMIT $1
    `;

    const result = await pool.query(query, [
      limit,
      offset,
      `%${cleanedSearch}%`,
    ]);
    return result.rows;
  } catch (err) {
    console.error("Error fetching projects by search:", err);
    throw err;
  }
}

//* Metodo interno privado
async function getProjectsFeatured(projectIds) {
  // Validación de los IDs de los proyectos
  if (!Array.isArray(projectIds) || projectIds.length === 0) {
    throw new Error("Debes proporcionar un array de IDs de proyectos");
  }

  // Convertir los IDs a un formato adecuado para la consulta SQL
  const projectIdPlaceholders = projectIds
    .map((_, index) => `$${index + 1}`)
    .join(", ");

  try {
    const query = `
      SELECT 
        projects.id, 
        projects.title, 
        projects.description, 
        projects.image, 
        projects.logo,
        projects.collaboration, 
        projects.tags,
        users.full_name AS author, 
        users.profile_image AS profileImage
      FROM projects
      JOIN users ON projects.author_id = users.id
      WHERE projects.id IN (${projectIdPlaceholders})
      ORDER BY ARRAY_POSITION($${projectIds.length + 1}, projects.id)
    `;

    // Pasar los IDs y el array de IDs como parámetros
    const result = await pool.query(query, [...projectIds, projectIds]);
    return result.rows;
  } catch (err) {
    console.error("Error fetching projects by IDs:", err);
    throw err;
  }
}

//* Método interno privado
async function getProjectByIdentifier(identifier) {
  // Validar que el identifier sea un string válido
  if (typeof identifier !== "string" || identifier.trim().length === 0) {
    throw new Error("El identificador alterno proporcionado no es válido.");
  }

  // Función para validar la estructura del identificador
  function validateIdentifierStructure(identifier) {
    const totalLengthExpected = 47; // 40 caracteres alfanuméricos + 7 guiones
    const groupSize = 5;
    const totalAlphaNumericChars = 40;

    // Verificar longitud total
    if (identifier.length !== totalLengthExpected) {
      return false;
    }

    // Verificar cantidad de guiones
    const dashCount = (identifier.match(/-/g) || []).length;
    if (dashCount !== 7) {
      return false;
    }

    // Verificar número de caracteres alfanuméricos
    const alphaNumericCount = (
      identifier.replace(/-/g, "").match(/[A-Za-z0-9]/g) || []
    ).length;
    if (alphaNumericCount !== totalAlphaNumericChars) {
      return false;
    }

    // Verificar estructura general (grupos de 5 caracteres separados por guiones)
    const regex = new RegExp(
      `^([A-Za-z0-9]{${groupSize}}-){7}[A-Za-z0-9]{${groupSize}}$`
    );
    return regex.test(identifier);
  }

  // Validar la estructura del identificador
  const isValidIdentifier = validateIdentifierStructure(identifier);
  if (!isValidIdentifier) {
    throw new Error("La estructura del identificador alterno no es válida.");
  }

  try {
    const query = `
      SELECT 
        projects.title, 
        projects.description, 
        projects.image, 
        projects.logo,
        projects.collaboration, 
        projects.tags,
        users.full_name AS author, 
        users.profile_image AS profileImage
      FROM projects
      JOIN users ON projects.author_id = users.id
      WHERE projects.identifier = $1
    `;

    // Realizar la consulta con el identifier
    const result = await pool.query(query, [identifier]);

    // Validar si se encontró el proyecto
    if (result.rows.length === 0) {
      throw new Error(
        "No se encontró un proyecto con el identificador proporcionado."
      );
    }

    return result.rows[0];
  } catch (err) {
    console.error(
      "Error al consultar el proyecto por identifier:",
      err.message
    );
    throw new Error(
      "Hubo un problema al buscar el proyecto. Por favor, intenta nuevamente."
    );
  }
}

export {
  getProjects,
  getProjectsByTag,
  getProjectsBySearch,
  getProjectsFeatured,
  getProjectByIdentifier
};

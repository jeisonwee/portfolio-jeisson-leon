const SVGComponent = ({ color }) => (
  <svg
    width={32}
    height={32}
    viewBox="0 0 32 32"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M16 32A16 16 0 1 0 4.865 4.51l3.34 3.447A11.2 11.2 0 1 1 16 27.2z"
      fill={color}
      fillOpacity={0.3}
    />
    <path
      d="M16 0a16 16 0 1 0 11.135 27.49l-3.34-3.447A11.2 11.2 0 1 1 16 4.8z"
      fill={color}
    />
    <circle
      cx={25.351}
      cy={25.875}
      r={2.4}
      transform="rotate(-44.108 25.35 25.875)"
      fill={color}
    />
    <circle
      cx={15.841}
      cy={2.401}
      r={2.4}
      transform="rotate(-90 15.841 2.4)"
      fill={color}
    />
  </svg>
);
export default SVGComponent;

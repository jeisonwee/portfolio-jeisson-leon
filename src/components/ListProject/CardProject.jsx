"use client"

import Link from "next/link";
import Image from "next/image";

// Estilos
import s from "./cardProject.module.css";

// Recursos
import MascPics from "./MascPics";

function CardProject({ info, langCategories }) {
  const {
    id,
    title,
    description,
    image,
    logo,
    author,
    profileimage,
    collaboration,
    tags,
  } = info;

  // Tag categorias
  const TagAssigned = ({ name }) => {
    return <span className={s.tag_assigned}>{langCategories[name]}</span>;
  };

  return (
    <li className={s.cont_card_project}>
      <Link className={s.link_project} href={`/projects/${id}`}>
        <section className={s.cont_img}>
          <MascPics
            className={s.imgs_project}
            imageUrl1={image}
            imageUrl2={logo}
            uniqueId={`project_${info.id}`}
          />
        </section>

        <section className={s.cont_info}>
          <article className={s.cont_authors}>
            <Image src={profileimage} alt="Profile" width={24} height={24} />
            <p>
              <b>{author}</b>{" "}
              {collaboration ? (
                <span>
                  con <b>{collaboration}</b>
                </span>
              ) : (
                ""
              )}
            </p>
          </article>
          <article className={s.cont_descri}>
            <h3>{title}</h3>
            <p>{description}</p>
          </article>
          <article className={s.cont_categories}>
            {tags.map((tag) => (
              <TagAssigned name={tag} key={tag} />
            ))}
          </article>
        </section>
      </Link>
    </li>
  );
}

export default CardProject;

/* const SVGComponent = ({className, imageUrl1, imageUrl2 }) => (

  <svg
    className={className}
    width={276}
    height={165}
    viewBox="0 0 276 165"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      d="M0.137695 20C0.137695 8.95433 9.092 0 20.1377 0H255.999C267.045 0 275.999 8.95431 275.999 20V63.1782C275.999 74.2239 267.045 83.1782 255.999 83.1782H218.758C207.712 83.1782 198.758 92.1325 198.758 103.178V144.897C198.758 155.943 189.803 164.897 178.758 164.897H20.1377C9.09199 164.897 0.137695 155.943 0.137695 144.897V20Z"
      fill="url(#pattern0_866_399)"
    />
    <path
      d="M20.1377 0.5H255.999C266.768 0.5 275.499 9.23045 275.499 20V63.1782C275.499 73.9477 266.768 82.6782 255.999 82.6782H218.758C207.436 82.6782 198.258 91.8563 198.258 103.178V144.897C198.258 155.667 189.527 164.397 178.758 164.397H20.1377C9.36813 164.397 0.637695 155.667 0.637695 144.897V20C0.637695 9.23047 9.36814 0.5 20.1377 0.5Z"
      stroke="#808080"
      strokeOpacity={0.16}
    />
    <rect
      x={209.792}
      y={94.8535}
      width={66.2067}
      height={70.0448}
      rx={12}
      fill="url(#pattern1_866_399)"
    />
    <rect
      x={210.292}
      y={95.3535}
      width={65.2067}
      height={69.0448}
      rx={11.5}
      stroke="#808080"
      strokeOpacity={0.16}
    />
    <defs>
      <pattern
        id="pattern0_866_399"
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use
          xlinkHref="#image0_866_399"
          transform="matrix(0.000259442 0 0 0.000434028 -0.0313369 0)"
        />
      </pattern>
      <pattern
        id="pattern1_866_399"
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use
          xlinkHref="#image1_866_399"
          transform="matrix(0.000979604 0 0 0.000925926 -0.0289859 0)"
        />
      </pattern>
      <image
        id="image0_866_399"
        width={4096}
        height={2304}
        xlinkHref={imageUrl1}
      />
      <image
        id="image1_866_399"
        width={1080}
        height={1080}
        xlinkHref={imageUrl2}
      />
    </defs>
  </svg>
);
export default SVGComponent; */

const SVGComponent = ({ className, imageUrl1, imageUrl2, uniqueId }) => (
  <svg
    className={className}
    width={276}
    height={165}
    viewBox="0 0 276 165"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      d="M0.137695 20C0.137695 8.95433 9.092 0 20.1377 0H255.999C267.045 0 275.999 8.95431 275.999 20V63.1782C275.999 74.2239 267.045 83.1782 255.999 83.1782H218.758C207.712 83.1782 198.758 92.1325 198.758 103.178V144.897C198.758 155.943 189.803 164.897 178.758 164.897H20.1377C9.09199 164.897 0.137695 155.943 0.137695 144.897V20Z"
      fill={`url(#pattern0_${uniqueId})`} // Unique ID here
    />
    <path
      d="M20.1377 0.5H255.999C266.768 0.5 275.499 9.23045 275.499 20V63.1782C275.499 73.9477 266.768 82.6782 255.999 82.6782H218.758C207.436 82.6782 198.258 91.8563 198.258 103.178V144.897C198.258 155.667 189.527 164.397 178.758 164.397H20.1377C9.36813 164.397 0.637695 155.667 0.637695 144.897V20C0.637695 9.23047 9.36814 0.5 20.1377 0.5Z"
      stroke="#808080"
      strokeOpacity={0.16}
    />
    <rect
      x={209.792}
      y={94.8535}
      width={66.2067}
      height={70.0448}
      rx={12}
      fill={`url(#pattern1_${uniqueId})`} // Unique ID here
    />
    <rect
      x={210.292}
      y={95.3535}
      width={65.2067}
      height={69.0448}
      rx={11.5}
      stroke="#808080"
      strokeOpacity={0.16}
    />
    <defs>
      <pattern
        id={`pattern0_${uniqueId}`} // Unique ID here
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use
          xlinkHref={`#image0_${uniqueId}`} // Unique ID here
          transform="matrix(0.000259442 0 0 0.000434028 -0.0313369 0)"
        />
      </pattern>
      <pattern
        id={`pattern1_${uniqueId}`} // Unique ID here
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use
          xlinkHref={`#image1_${uniqueId}`} // Unique ID here
          transform="matrix(0.000979604 0 0 0.000925926 -0.0289859 0)"
        />
      </pattern>
      <image
        id={`image0_${uniqueId}`} // Unique ID here
        width={4096}
        height={2304}
        xlinkHref={imageUrl1}
      />
      <image
        id={`image1_${uniqueId}`} // Unique ID here
        width={1080}
        height={1080}
        xlinkHref={imageUrl2}
      />
    </defs>
  </svg>
);

export default SVGComponent;










const SVGComponent = ({ color }) => (
  <svg
    width={16}
    height={16}
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1 8h3m4-7v3m7 4h-3m-4 7v-3m-4.95.95 2.121-2.122M3.05 3.05l2.12 2.123m7.78-2.123-2.12 2.123m2.12 7.777-2.121-2.122"
      stroke={color}
      strokeWidth={1.5}
      strokeLinecap="round"
    />
  </svg>
);
export default SVGComponent;
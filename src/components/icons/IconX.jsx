const SVGComponent = ({ color, width = 10, height = 10 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 10 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7.828 9.242a1 1 0 1 0 1.414-1.414L6.414 5l2.828-2.828A1 1 0 1 0 7.828.758L5 3.586 2.171.757A1 1 0 0 0 .757 2.171L3.586 5 .757 7.829a1 1 0 0 0 1.414 1.414L5 6.414z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
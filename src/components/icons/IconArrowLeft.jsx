const SVGComponent = ({ color, width = 10, height = 16, style }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 10 16"
    style={style}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9.261.37a1.263 1.263 0 0 1 0 1.786L3.418 8l5.843 5.844a1.263 1.263 0 1 1-1.786 1.786L.738 8.893a1.263 1.263 0 0 1 0-1.786L7.475.37a1.263 1.263 0 0 1 1.786 0"
      fill={color}
    />
  </svg>
);

export default SVGComponent;

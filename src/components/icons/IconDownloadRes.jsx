const SVGComponent = ({ color, width = 21, height = 22, className }) => (
  <svg
    width={width}
    height={height}
    className={className}
    viewBox="0 0 21 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      clipRule="evenodd"
      d="M6.744 1h7.361q.116 0 .228.015a5.733 5.733 0 0 1 5.334 5.7v8.572A5.733 5.733 0 0 1 13.923 21H6.744A5.733 5.733 0 0 1 1 15.285v-8.57A5.733 5.733 0 0 1 6.744 1"
      stroke={color}
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M14.337 17.993a1 1 0 1 0 0-2zm-8-2a1 1 0 1 0 0 2zM19.668 7.71a1 1 0 1 0 0-2zm-5.333-1h-1a1 1 0 0 0 1 1zm1-5.7a1 1 0 0 0-2 0zm-1.623 10.023a1 1 0 0 0-1.414-1.414zm-4.08 1.253a1 1 0 1 0 1.414 1.414zM9.629 13.7a1 1 0 1 0 1.414-1.414zM8.376 9.62a1 1 0 1 0-1.414 1.413zm.959 3.374a1 1 0 1 0 2 0zm2-8a1 1 0 1 0-2 0zm3.002 11h-8v2h8zM19.668 5.71h-5.333v2h5.333zm-4.333 1v-5.7h-2v5.7zm-3.037 2.91-2.666 2.666 1.414 1.414 2.666-2.667zm-1.255 2.666L8.376 9.619l-1.414 1.414L9.63 13.7zm.292.708v-8h-2v8z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;

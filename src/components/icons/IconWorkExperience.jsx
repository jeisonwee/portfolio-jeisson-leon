const SVGComponent = ({ color }) => (
  <svg
    width={27}
    height={24}
    viewBox="0 0 27 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M13.333 5.333V0H0v24h26.667V5.333zm-2.666 16h-8v-2.666h8zm0-5.333h-8v-2.667h8zm0-5.333h-8V8h8zm0-5.334h-8V2.667h8zm13.333 16H13.333V8H24zm-2.667-10.666H16v2.666h5.333zm0 5.333H16v2.667h5.333z"
      fill={color}
      fillOpacity={0.92}
    />
  </svg>
);
export default SVGComponent;

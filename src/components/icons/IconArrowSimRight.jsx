const SVGComponent = ({ color, className }) => (
  <svg
    width={6}
    height={12}
    className={className}
    viewBox="0 0 6 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M.24 1.058a.82.82 0 0 1 1.157 0L5.76 5.42c.32.32.32.838 0 1.157l-4.363 4.364A.818.818 0 0 1 .24 9.785L4.025 6 .24 2.215a.82.82 0 0 1 0-1.157"
      fill={color}
    />
  </svg>
);
export default SVGComponent;

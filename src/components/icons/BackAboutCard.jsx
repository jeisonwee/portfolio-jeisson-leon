"use client"

import Image from "next/image";
import { useTheme } from "@/components/ThemeControl";

import BackAboutDark from "@/img/back_about_dark.png";
import BackAboutLight from "@/img/back_about_light.png";

const CompImgTheme = ({ className }) => (
  <Image
    className={className}
    src={useTheme() === "dark" ? BackAboutDark : BackAboutLight}
    alt="Background"
  />
);
export default CompImgTheme;

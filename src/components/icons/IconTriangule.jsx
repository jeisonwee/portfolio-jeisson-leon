const SVGComponent = ({ color, className }) => (
  <svg width={14} height={9} className={className} viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg" > <path d="M7.756 8.127a1 1 0 0 1-1.512 0L.638 1.655A1 1 0 0 1 1.394 0h11.211a1 1 0 0 1 .756 1.655z" fill={color} /> </svg>
);
export default SVGComponent;

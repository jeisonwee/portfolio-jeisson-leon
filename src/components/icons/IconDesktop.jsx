const SVGComponent = ({ color }) => (
  <svg
    width={20}
    height={20}
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.508 0H5.48A5.016 5.016 0 0 0 .6 5.127v6.147a5.016 5.016 0 0 0 4.909 5.127h9.009a5.016 5.016 0 0 0 4.884-5.127V5.127A5.016 5.016 0 0 0 14.492 0zm-2.91 5.09A3.016 3.016 0 0 1 5.523 2h8.956A3.016 3.016 0 0 1 17.4 5.09v6.221a3.016 3.016 0 0 1-2.924 3.09H5.522a3.016 3.016 0 0 1-2.923-3.115V5.09M4 18.002a1 1 0 1 0 0 2h12a1 1 0 1 0 0-2z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
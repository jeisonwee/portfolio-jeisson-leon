const SVGComponent = ({ color }) => (
  <svg
    width={16}
    height={16}
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M1.5 7.546a6 6 0 1 1 11.999 0 6 6 0 0 1-11.999 0m6-7.5a7.5 7.5 0 1 0 0 15 7.5 7.5 0 0 0 0-15m.749 3.45a.75.75 0 0 0-1.5 0v4.05c0 .284.16.543.414.67l2.7 1.35a.75.75 0 0 0 .671-1.341L8.25 7.082z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
const SVGComponent = ({ color, width, height }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 16 17"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13.474 7.606a5.895 5.895 0 1 1-11.79 0 5.895 5.895 0 0 1 11.79 0m.027 4.731a7.579 7.579 0 1 0-1.19 1.19l2.251 2.254a.842.842 0 0 0 1.191-1.191z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
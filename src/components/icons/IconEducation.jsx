const SVGComponent = ({ color }) => (
  <svg
    width={30}
    height={24}
    viewBox="0 0 30 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.333 13.573v5.334L14.667 24 24 18.907v-5.334l-9.333 5.094zM14.667 0 0 8l14.667 8 12-6.547v9.214h2.666V8z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;

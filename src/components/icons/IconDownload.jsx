const SVGComponent = ({ color }) => (
  <svg
    width={16}
    height={16}
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.857 1.143a1 1 0 1 0-2 0V7.87L4.755 5.768a1 1 0 0 0-1.414 1.414l3.781 3.782a.997.997 0 0 0 1.458.012l3.794-3.794a1 1 0 1 0-1.414-1.414L8.857 7.871zM1 9.285a1 1 0 0 1 1 1v3.048a.524.524 0 0 0 .524.524H13.19a.524.524 0 0 0 .523-.524v-3.048a1 1 0 0 1 2 0v3.048a2.524 2.524 0 0 1-2.524 2.524H2.524A2.524 2.524 0 0 1 0 13.333v-3.048a1 1 0 0 1 1-1"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
"use client"

import { useTheme } from "../ThemeControl";
import Image from "next/image";

import IconLionTDark from "@/img/icons/icon_lionteam.svg";
import IconLionTLight from "@/img/icons/icon_lionteam_light.svg";

const LionTeamComp = () => (
  <Image src={useTheme() === "dark" ? IconLionTDark : IconLionTLight} alt="LionTeam" />
);
export default LionTeamComp;

const SVGComponent = ({ color, clase }) => (
  <svg
    width={16}
    height={16}
    className={clase}
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M9.283 12.941a.75.75 0 1 0 1.034 1.086zm6.434-4.056a.75.75 0 1 0-1.034-1.087zm-1.034-.001a.75.75 0 0 0 1.034-1.086zm-4.366-6.23a.75.75 0 1 0-1.034 1.087zM15.2 9.09a.75.75 0 0 0 0-1.5zM.8 7.59a.75.75 0 0 0 0 1.5zm9.517 6.438 5.4-5.143-1.034-1.087-5.4 5.143zm5.4-6.23-5.4-5.143L9.283 3.74l5.4 5.143zM15.2 7.59H.8v1.5h14.4z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
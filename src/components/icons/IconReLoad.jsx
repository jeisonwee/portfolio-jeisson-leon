const SVGComponent = ({ width = 17, height = 16, className, color }) => (
  <svg
    width={width}
    height={height}
    className={className}
    viewBox="0 0 17 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M2.1 8a6.4 6.4 0 0 1 6.4-6.4c2.014 0 3.783.93 4.946 2.395l-1.344-.004a.8.8 0 1 0-.004 1.6l2.754.008q.046.003.093 0l.353.001a.8.8 0 0 0 .802-.8V1.6a.8.8 0 0 0-1.6 0v1.16A7.86 7.86 0 0 0 8.5 0a8 8 0 0 0-8 8 .8.8 0 1 0 1.6 0m12.8 0a6.4 6.4 0 0 1-6.4 6.4 6.28 6.28 0 0 1-4.945-2.395l1.343.004a.8.8 0 1 0 .005-1.6L2.148 10.4a1 1 0 0 0-.092 0h-.353a.8.8 0 0 0-.803.8v3.2a.8.8 0 0 0 1.6 0v-1.16a7.86 7.86 0 0 0 6 2.76 8 8 0 0 0 8-8 .8.8 0 0 0-1.6 0"
      fill={color}
    />
  </svg>
);
export default SVGComponent;

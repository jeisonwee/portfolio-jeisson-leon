const SVGComponent = ({ color }) => (
  <svg
    width={21}
    height={24}
    viewBox="0 0 21 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="m10.392 0 10.393 6v12l-10.393 6L0 18V6zM2.31 8.533v8.134l6.929 4v-8.134zm16.166 0-6.928 4v8.134l6.928-4zm-8.083-5.866L3.58 6.6l6.812 3.933L17.205 6.6z"
      fill={color}
    />
  </svg>
);
export default SVGComponent;
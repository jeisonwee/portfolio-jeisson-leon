"use client";

import { useEffect, useRef } from "react";

// Estilos
import s from "./videoPlayer.module.css";

const VideoPlayer = ({
  src,
  autoPlay = false,
  controls = true,
  loop = false,
  className,
  style
}) => {
  const videoRef = useRef(null);

  useEffect(() => {
    //? Puedes usar IntersectionObserver para hacer lazy loading
    const videoElement = videoRef.current;
    const onIntersection = (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          videoElement.play();
        } else {
          videoElement.pause();
        }
      });
    };

    const observer = new IntersectionObserver(onIntersection, {
      threshold: 0.5,
    });

    if (videoElement) observer.observe(videoElement);

    return () => {
      if (videoElement) observer.unobserve(videoElement);
    };
  }, []);

  return (
    <video
      className={`${s.video_player} ${className}`}
      ref={videoRef}
      autoPlay={autoPlay}
      controls={controls}
      loop={loop}
      muted
      style={style}
      preload="metadata"
    >
      <source src={src} type="video/mp4" />
      No HTML5 videos supported.
    </video>
  );
};

export default VideoPlayer;

import Image from "next/image";
import Link from "next/link";
import { langUser } from "@/scripts/langConfig";
import { ButtonLetsTalk } from "./HeaderControl";

// Estilos
import s from "@/css/goToWork.module.css";

// Recursos
import ImgBackColors from "@/img/img_back_colors.webp";
import IconCommunity from "@/img/icons/icon_comunication.svg";

const GoToWork = () => {
  const allLang = langUser("home");

  return (
    <article className={s.page_sec_5}>
      <div className={s.sec5_container}>
        <div className={s.sec5_sec1}>
          <Image
            src={IconCommunity}
            className={s.icon_sec5}
            alt="Comunication"
          />
          <h3>{allLang.LetsWork.title}</h3>
          <ButtonLetsTalk
            className={s.btn_talk}
            lang={allLang.LetsWork.btnWork}
          />
        </div>

        <div className={s.sec5_sec2}>
          <span />
          <div className={s.cont_icons_sec5}>
            <Link
              href="mailto:jeissonwee.dev@gmail.com?subject=💙Hey Jei!"
              className={s.btn_commu}
            >
              <Image
                src="/icons/icon_gmail.svg"
                width={21}
                height={16}
                alt="Gmail"
              />
            </Link>
            <Link
              href="mailto:jeissonwee.dev@gmail.com?subject=💙Hey Jei!"
              className={s.btn_commu}
            >
              <Image
                src="/icons/icon_gmail.svg"
                width={21}
                height={16}
                alt="Gmail"
              />
            </Link>
            <Link href="https://t.me/@JeissonWee" className={s.btn_commu}>
              <Image
                src="/icons/icon_telegram.svg"
                width={20}
                height={20}
                alt="Telegram"
              />
            </Link>
            <Link
              href="https://www.linkedin.com/in/jeisson-le%C3%B3n-53b05a1a8/"
              className={s.btn_commu}
            >
              <Image
                src="/icons/icon_linkedin.svg"
                width={20}
                height={20}
                alt="Linkedin"
              />
            </Link>
          </div>
        </div>

        <p className={s.tx_sec5_bottom}>{allLang.LetsWork.bottomTx}</p>
      </div>

      <Image src={ImgBackColors} className={s.img_back_colors} alt="Colores" />
    </article>
  );
};
export default GoToWork;

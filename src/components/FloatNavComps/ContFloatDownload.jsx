import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

// Estilos
import s from "./contFloatDownload.module.css";

import IconDownloadRes from "@/components/icons/IconDownloadRes";
import IconFileColorizer from "@/img/icons/icon_file_colorizer.png";
import IconTrian from "@/components/icons/IconTriangule";
import IconX from "@/components/icons/IconX";
import IconPdf from "@/components/icons/IconPdf";
import IconWord from "@/components/icons/IconWord";
import IconEmail from "@/components/icons/IconEmail";
import IconNubeDown from "@/components/icons/IconNubeDown";
import IconArrowSim from "@/components/icons/IconArrowSimRight";

import IconEmailColorizer from "@/img/icons/icon_email_colorizer.svg";
import IconLoading from "@/components/IconLoading";

import IconReadyColor from "@/img/icons/icon_ready_color.svg";
import IconErrorColor from "@/img/icons/icon_error_color.svg";

const ContFloatDownload = ({ info, funcClose, lang }) => {
  const [sendEmail, setSendEmail] = useState(false);

  const BtnClose = () => {
    return (
      <button className={s.btn_close_float} onClick={() => funcClose("modal1")}>
        <IconX color="var(--cl-x)" />
      </button>
    );
  };

  const BtnLinks = ({ children, onClick }) => {
    return (
      <button className={s.btn_links} onClick={onClick}>
        {children}
      </button>
    );
  };

  const triggerDownload = (url) => {
    const link = document.createElement("a");
    link.href = url;
    link.download = "JeissonLeonCV";
    link.click();
  };
  const fetchDownloadUrl = async (type) => {
    try {
      const response = await fetch(`/api/resumeDown?type=${type}&lang=${lang}`);

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      const data = await response.json();
      triggerDownload(data.downloadUrl);
    } catch (error) {
      console.error("Failed to fetch download URL", error);
    }
  };

  const InitInfo = () => {
    return (
      <>
        <section className={s.cont_float_sec_1}>
          <Image src={IconFileColorizer} alt="IconColor" />
          <div>
            <h3>{info.contFloat.secInit.title}</h3>
            <p>*{info.contFloat.secInit.subTitle}*</p>
          </div>
        </section>

        <section className={s.cont_float_sec_2}>
          <IconDownloadRes
            className={s.img_download}
            color={"var(--color-contrast-800)"}
          />
          <ul className={s.cont_list_info}>
            <li>
              <p>{info.contFloat.secInit.file}:</p> <b>Jeisson Leon Res</b>
            </li>
            <li>
              <p>{info.contFloat.secInit.size}:</p> <b>300KB</b>
            </li>
            <li className={s.cont_links}>
              <p>{info.contFloat.secInit.resBasic}:</p>
              <div>
                <Link href="#">EspañolRes</Link>
                <Link href="#">EnglishRes</Link>
              </div>
            </li>
          </ul>
        </section>

        <section className={s.cont_float_sec_3}>
          <BtnLinks onClick={() => fetchDownloadUrl("pdf")}>
            <div>
              <IconPdf color="var(--cl-font)" />
              <p>PDF - Adobe</p>
            </div>
            <IconNubeDown color="var(--cl-font)" />
          </BtnLinks>
          <BtnLinks onClick={() => fetchDownloadUrl("word")}>
            <div>
              <IconWord color="var(--cl-font)" />
              <p>DOCX - Word</p>
            </div>
            <IconNubeDown color="var(--cl-font)" />
          </BtnLinks>

          <BtnLinks onClick={() => setSendEmail(true)}>
            <div>
              <IconEmail color="var(--cl-font)" />
              <p>{info.contFloat.secInit.btnSendEmail}</p>
            </div>
            <IconArrowSim className={s.ico_a_s} color="var(--cl-font)" />
          </BtnLinks>
        </section>
      </>
    );
  };

  const SendEmailSec = () => {
    // Estado del proceso de envio del correo
    const [stateProcess, setStateProcess] = useState(0);
    const [emailBack, setEmailBack] = useState("");

    // Parte 1 ------------------
    const InitPartEmail = () => {
      // Estados de la informacion ingresada
      const [selectedCheckbox, setSelectedCheckbox] = useState(null);
      const [email, setEmail] = useState("");
      const [emailValid, setEmailValid] = useState(false);

      // Verificaciones de email
      const handleEmailChange = (e) => {
        const emailValue = e.target.value;
        setEmail(emailValue);
        setEmailValid(validateEmail(emailValue));
      };
      const validateEmail = (email) => {
        const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return re.test(email);
      };

      const handleCheckboxChange = (checkboxId) => {
        setSelectedCheckbox(
          selectedCheckbox === checkboxId ? null : checkboxId
        );
      };

      const handleButtonClick = async () => {
        if (selectedCheckbox && emailValid) {
          const data = {
            email,
            type: selectedCheckbox,
            lang,
          };

          setStateProcess(1); // Cambia el estado a "cargando"

          try {
            const response = await fetch("/api/sendEmail", {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
            });

            if (response.ok) {
              let res = await response.json();
              setEmailBack(res.e);
              setStateProcess(2); // Cambia el estado a "exitoso"
            } else {
              setStateProcess(3); // Cambia el estado a "error"
            }
          } catch (error) {
            console.error("Error al enviar los datos:", error);
            setStateProcess(3); // Cambia el estado a "error"
          }
        } else {
          console.error("What u fucking doing bitch?, Are u stupid maybe?");
        }
      };

      // Iconos de checkboxs
      const uncheckedSVG = (
        <svg
          width={16}
          height={16}
          viewBox="0 0 16 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M13.657 2.343a8 8 0 0 0-11.314 0 8 8 0 0 0 0 11.314 8 8 0 0 0 11.314 0 8 8 0 0 0 0-11.314m-.943 10.37a6.666 6.666 0 1 1-9.428-9.427 6.666 6.666 0 0 1 9.428 0 6.666 6.666 0 0 1 0 9.428"
            fill="#F05"
          />
        </svg>
      );
      const checkedSVG = (
        <svg
          width={16}
          height={16}
          viewBox="0 0 16 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M2.343 2.343a8 8 0 0 1 11.314 0 8 8 0 0 1 0 11.314 8 8 0 0 1-11.314 0 8 8 0 0 1 0-11.314m.943 10.371a6.666 6.666 0 1 0 9.428-9.428 6.666 6.666 0 1 0-9.428 9.428m4.047-3.657 4.196-4.195a.667.667 0 0 1 .942.942l-4.666 4.667a.667.667 0 0 1-.943 0L4.195 7.804a.667.667 0 0 1 .943-.942z"
            fill="#F05"
          />
        </svg>
      );

      return (
        <>
          <section className={`${s.cont_email_sec1} ${s.aniInit}`}>
            <Image src={IconEmailColorizer} alt="IconColor" />
            <h3>{info.contFloat.secSecond.title}</h3>
          </section>

          <section className={`${s.cont_email_sec2} ${s.aniInit}`}>
            <article className={s.sec2_input}>
              <label htmlFor="email">
                {info.contFloat.secSecond.label}
                <span className={s.ico_requi}> *</span>
              </label>

              <input
                id="email"
                type="email"
                placeholder={info.contFloat.secSecond.placeholder}
                value={email}
                onChange={handleEmailChange}
                required
                style={{
                  borderColor: `${
                    !emailValid && email ? "var(--color-error)" : ""
                  } ${emailValid && email ? "var(--color-ready)" : ""}`,
                }}
              />
            </article>

            <article className={s.sec3_preferrer}>
              <label>
                {info.contFloat.secSecond.label2}
                <span className={s.ico_requi}> *</span>
              </label>

              <label className={s.cont_option}>
                <input
                  type="checkbox"
                  checked={selectedCheckbox === "pdf"}
                  onChange={() => handleCheckboxChange("pdf")}
                  style={{ display: "none" }}
                />
                <div style={{ display: "flex", alignItems: "center" }}>
                  {selectedCheckbox === "pdf" ? checkedSVG : uncheckedSVG}
                </div>
                <p>(.pdf) PDF - Adobe</p>
              </label>

              <label className={s.cont_option}>
                <input
                  type="checkbox"
                  checked={selectedCheckbox === "word"}
                  onChange={() => handleCheckboxChange("word")}
                  style={{ display: "none" }}
                />
                <div style={{ display: "flex", alignItems: "center" }}>
                  {selectedCheckbox === "word" ? checkedSVG : uncheckedSVG}
                </div>
                <p>(.docx) WORD - Microsoft</p>
              </label>
            </article>

            <button
              className={`${s.btn_send} ${
                !selectedCheckbox || !emailValid ? "" : s.act_btnsend
              }`}
              onClick={handleButtonClick}
              disabled={!selectedCheckbox || !emailValid}
            >
              {info.contFloat.secSecond.btnSend}
            </button>

            <p className={s.tx_bottom}>
              {info.contFloat.secSecond.txBottom.first}{" "}
              {
                <Link className={s.link_bottom} href="/contact">
                  {info.contFloat.secSecond.txBottom.contact}
                </Link>
              }
              {info.contFloat.secSecond.txBottom.second}{" "}
              {
                <Link className={s.link_bottom} href="/contact">
                  {info.contFloat.secSecond.txBottom.priva}
                </Link>
              }
              .
            </p>
          </section>
        </>
      );
    };

    // Parte 2 (Procesando) ------------
    const ProcessingLoading = () => {
      return (
        <section className={s.cont_processing}>
          <IconLoading color="var(--color-primary)" />
        </section>
      );
    };

    // Parte 3 (Proceso satisfactorio) ------------
    const SuccesfullyComp = () => {
      return (
        <section className={`${s.cont_sec_success} ${s.aniInit}`}>
          <article className={s.succ_sec1}>
            <Image src={IconReadyColor} alt="Ready" />
            <h3>{info.contFloat.secThree.title}</h3>
          </article>
          <article className={s.succ_sec2}>
            <p>
              {info.contFloat.secThree.body} <span>{emailBack}</span>
            </p>
          </article>

          <article className={s.succ_sec3}>
            <button
              className={s.btn_back_send}
              onClick={() => setStateProcess(0)}
            >
              {info.contFloat.secThree.button}
            </button>
          </article>

          <article>
            <p className={s.tx_bottom}>
              {info.contFloat.secThree.txBottom}{" "}
              <Link className={s.link_bottom} href="/contact">
                {info.contFloat.secSecond.txBottom.contact}
              </Link>
            </p>
          </article>
        </section>
      );
    };

    // Parte 4 (ERROR) ------------------
    const ErrorProcess = () => {
      return (
        <section className={`${s.cont_sec_error} ${s.aniInit}`}>
          <article className={s.error_sec1}>
            <Image src={IconErrorColor} alt="Error" />
            <h3>{info.contFloat.secFou.title}</h3>
          </article>

          <article className={s.succ_sec2}>
            <p>{info.contFloat.secFou.body}</p>
          </article>

          <article className={s.succ_sec3}>
            <button
              className={s.btn_back_send}
              onClick={() => setStateProcess(0)}
            >
              {info.contFloat.secFou.button}
            </button>
          </article>

          <article>
            <p className={s.tx_bottom}>
              {info.contFloat.secFou.txBottom}{" "}
              <Link className={s.link_bottom} href="/contact">
                {info.contFloat.secSecond.txBottom.contact}
              </Link>
            </p>
          </article>
        </section>
      );
    };

    return (
      <>
        <button
          className={`${s.btn_back} ${s.aniInit}`}
          onClick={() => setSendEmail(false)}
        >
          <IconArrowSim className={s.ico_back} color="var(--cl-back-e)" />
        </button>

        {stateProcess === 0 && <InitPartEmail />}
        {stateProcess === 1 && <ProcessingLoading />}
        {stateProcess === 2 && <SuccesfullyComp />}
        {stateProcess === 3 && <ErrorProcess />}
      </>
    );
  };

  return (
    <>
      <div className={s.down_cont_float}>
        <BtnClose />
        {sendEmail ? <SendEmailSec /> : <InitInfo />}
        <IconTrian
          className={s.ico_triangule}
          color="var(--color-contrast-160)"
        />
      </div>

      <div className={s.back_float} onClick={() => funcClose("modal1")} />
    </>
  );
};

export default ContFloatDownload;

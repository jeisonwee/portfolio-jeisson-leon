import Image from "next/image";

// Estilos
import s from "./contFloatLang.module.css";

// Recursos
import IconX from "@/components/icons/IconX";
import IconLangs from "@/components/icons/IconLangs";
import IconTrian from "@/components/icons/IconTriangule";
import IconArrowLeft from "@/components/icons/IconArrowLeft";

const BtnChangeLang = ({ lang, ico, typeLang, title }) => {
  const handleChangeLang = async () => {
    if (typeLang === lang) {
      return false;
    }

    try {
      const response = await fetch(`/api/chaLang?value=${typeLang}`);
      const data = await response.json();

      if (response.ok) {
        window.location.reload();
      } else {
        setError(data.error);
      }
    } catch (err) {
      console.error("Error changing lang :(");
    }
  };

  return (
    <button
      className={`${s.btn_changelang} ${
        lang === typeLang ? s.selectedBtn : ""
      }`}
      onClick={() => handleChangeLang(typeLang)}
    >
      <section className={s.changelang_sec}>
        <Image src={ico} width={20} height={20} alt="Ico" />
        <p>{title}</p>
      </section>
      <IconArrowLeft
        width={6}
        height={12}
        color="var(--cl-chBtn)"
        style={{ transform: "rotate(180deg)" }}
      />
    </button>
  );
};

const ContFloatLang = ({ lang, languague, funcClose }) => {
  const BtnClose = () => {
    return (
      <button className={s.btn_close_float} onClick={() => funcClose("modal2")}>
        <IconX color="var(--cl-x)" />
      </button>
    );
  };

  return (
    <>
      <div className={s.lang_cont_float}>
        <BtnClose />
        <section className={s.lang_float_sec_1}>
          <IconLangs color="var(--color-primary)" />
          <h3>{languague}</h3>
        </section>

        <section className={s.lang_float_sec_2}>
          <BtnChangeLang
            lang={lang}
            ico="/countries/spain.svg"
            typeLang="es"
            title="Español"
          />
          <BtnChangeLang
            lang={lang}
            ico="/countries/united_kingdom.svg"
            typeLang="en"
            title="English"
          />
          {/* <BtnChangeLang
          lang={lang}
          ico="/countries/france.svg"
          typeLang="fr"
          title="Français"
        /> */}
        </section>

        <IconTrian
          className={s.ico_triangule}
          color="var(--color-contrast-160)"
        />
      </div>

      <div className={s.back_float} onClick={() => funcClose("modal2")} />
    </>
  );
};

export default ContFloatLang;

"use client";

import Image from "next/image";
import { useState } from "react";

// Estilos
import s from "@/css/floatNav.module.css";

// Componentes
import ContFloatDownload from "./FloatNavComps/ContFloatDownload";
import ContFloatLang from "./FloatNavComps/ContFloatLang";

// Recursos
import IconDownloadRes from "@/components/icons/IconDownloadRes";

import IconArrowLeft from "@/components/icons/IconArrowLeft";

import { ChangeTheme } from "./ThemeControl";

const FloatNav = ({ info, lang }) => {
  const initModals = {
    modal1: false,
    modal2: false,
  };
  const [modals, setModals] = useState(initModals);

  // Arbol de destruccion conjunta de modales
  const handleModalToggle = (modalName) => {
    setModals((prevState) => {
      // Crear un nuevo estado con todos los modales en false
      const newState = Object.keys(prevState).reduce((acc, key) => {
        acc[key] = false;
        return acc;
      }, {});

      // Solo el modal clicado se pone en true
      newState[modalName] = !prevState[modalName];
      return newState;
    });
  };

  return (
    <>
      <nav className={s.nav_float}>
        <div className={s.cont_download}>
          <button
            className={s.btn_download}
            onClick={() => handleModalToggle("modal1")}
          >
            <IconDownloadRes
              className={s.img_download}
              color={"var(--color-contrast-900)"}
            />
            <section className={s.down_info}>
              <h1>{info.downloadCv.title}</h1>
              <p>JeissonLeon.pdf</p>
            </section>
            <section className={s.down_simbol}>
              <IconArrowLeft
                width={10}
                height={12}
                style={{ transform: `rotate(${modals.modal1 ? 90 : 180}deg)` }}
                color="var(--color-contrast-700-t)"
              />
            </section>
          </button>
          <div className={s.modal_pc}>
            {modals.modal1 && (
              <ContFloatDownload
                lang={lang}
                info={info.downloadCv}
                funcClose={handleModalToggle}
              />
            )}
          </div>
        </div>

        <div className={s.cont_langs}>
          <button
            className={s.btn_lang}
            onClick={() => handleModalToggle("modal2")}
          >
            <section className={s.lang_info}>
              <Image
                src={`/countries/${info.langPic}`}
                width={18}
                height={18}
                alt="Country"
              />
              <p>{info.lang}</p>
            </section>

            <section className={s.lang_simbol}>
              <IconArrowLeft
                width={10}
                height={12}
                style={{ transform: `rotate(${modals.modal2 ? 90 : 180}deg)` }}
                color="var(--color-contrast-700-t)"
              />
            </section>
          </button>
          <div className={s.modal_pc}>
            {modals.modal2 && (
              <ContFloatLang
                lang={lang}
                funcClose={handleModalToggle}
                languague={info.langTitle}
              />
            )}
          </div>
        </div>

        <ChangeTheme className={s.btn_theme} />
      </nav>

      {/*//? Aqui se controla la vista para telefonos, se controla basicamente con el uso de CSS se condiciona la renderizacion de modal_mobi y modal_pc */}
      <div className={s.modal_mobi}>
        {modals.modal1 && (
          <ContFloatDownload
            lang={lang}
            info={info.downloadCv}
            funcClose={handleModalToggle}
          />
        )}
        {modals.modal2 && (
          <ContFloatLang
            lang={lang}
            funcClose={handleModalToggle}
            languague={info.langTitle}
          />
        )}
      </div>
    </>
  );
};

export default FloatNav;

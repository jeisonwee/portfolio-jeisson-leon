"use client";

import Image from "next/image";
import { createContext, useContext, useState, useEffect } from "react";

// Estilos
import s from "@/css/themeControl.module.css";

// Recursos
import IconDark from "@/img/icons/icon_dark.svg";
import IconLight from "@/img/icons/icon_light.svg";

// Metodos de uso general para el combio de temas
export let toggleTheme;

// Variables de colores y gamas globales
const darkTheme = `
  :root {
    --color-back: #0a0a0a;      
    --color-back-800-t: hsla(0, 0%, 4%, 0.8);
    --color-back-700-t: hsla(0, 0%, 4%, 0.72);
    --color-back-500-t: hsla(0, 0%, 4%, 0.5);
    --color-back-700-8-t: hsla(0, 0%, 8%, 0.72);

    --color-contrast-900: hsl(0, 0%, 92%);
    --color-contrast-900-t: rgba(255, 255, 255, 0.92);
    --color-contrast-800: hsl(0, 0%, 80%);
    --color-contrast-800-t: rgba(255, 255, 255, 0.8);
    --color-contrast-700: hsl(0, 0%, 72%);
    --color-contrast-700-t: rgba(255, 255, 255, 0.72);
    --color-contrast-500: hsl(0, 0%, 50%);
    --color-contrast-500-t: rgba(255, 255, 255, 0.5);
    --color-contrast-200: hsl(0, 0%, 24%);
    --color-contrast-200-t: rgba(255, 255, 255, 0.24);
    --color-contrast-160: hsl(0, 0%, 16%);
    --color-contrast-160-t: rgba(255, 255, 255, 0.16);
    --color-contrast-120: hsl(0, 0%, 12%);
    --color-contrast-120-t: rgba(255, 255, 255, 0.12);
    --color-contrast-120-700-t: hsla(0, 0%, 12%, 0.72);
    --color-contrast-80: hsl(0, 0%, 8%);
    --color-contrast-80-t: rgba(255, 255, 255, 0.08);
    --color-contrast-60: hsl(0, 0%, 6%);
    --color-contrast-60-t: rgba(255, 255, 255, 0.06);
    --color-contrast-40: hsl(0, 0%, 4%);
    --color-contrast-40-t: rgba(255, 255, 255, 0.04);
    --color-contrast-20-t: rgba(255, 255, 255, 0.02);
    
    --color-comu: hsla(0, 0%, 4%, 0.92);
  }
`;
const lightTheme = `
  :root {
    --color-back: hsl(0, 0%, 94%);
    --color-back-800-t: hsla(0, 0%, 94%, 0.8);
    --color-back-700-t: hsla(0, 0%, 94%, 0.72);
    --color-back-500-t: hsla(0, 0%, 94%, 0.5);
    --color-back-700-8-t: hsla(0, 0%, 94%, 0.72);

    --color-contrast-900: hsl(0, 0%, 8%);
    --color-contrast-900-t: rgba(0, 0, 0, 0.92);
    --color-contrast-800: hsl(0, 0%, 20%);
    --color-contrast-800-t: rgba(0, 0, 0, 0.8);
    --color-contrast-700: hsl(0, 0%, 28%);
    --color-contrast-700-t: rgba(0, 0, 0, 0.72);
    --color-contrast-500: hsl(0, 0%, 50%);
    --color-contrast-500-t: rgba(0, 0, 0, 0.5);
    --color-contrast-200: hsl(0, 0%, 76%);
    --color-contrast-200-t: rgba(0, 0, 0, 0.24);
    --color-contrast-160: hsl(0, 0%, 84%);
    --color-contrast-160-t: rgba(0, 0, 0, 0.16);
    --color-contrast-120: hsl(0, 0%, 88%);
    --color-contrast-120-t: rgba(0, 0, 0, 0.12);
    --color-contrast-120-700-t: hsla(0, 0%, 88%, 0.72);
    --color-contrast-80: hsl(0, 0%, 92%);
    --color-contrast-80-t: rgba(0, 0, 0, 0.08);
    --color-contrast-60: hsl(0, 0%, 94%);
    --color-contrast-60-t: rgba(0, 0, 0, 0.06);
    --color-contrast-40: hsl(0, 0%, 96%);
    --color-contrast-40-t: rgba(0, 0, 0, 0.04);
    --color-contrast-20: hsl(0, 0%, 96%);
    --color-contrast-20-t: rgba(0, 0, 0, 0.02);

    --color-comu: hsla(0, 0%, 94%, 0.92);
  }
`;

const ThemeContext = createContext();

export const useTheme = () => useContext(ThemeContext);

// Componente de control para temas
export const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState("dark");

  useEffect(() => {
    const savedTheme = localStorage.getItem("theme");
    if (savedTheme) {
      setTheme(savedTheme);
    } else {
      const systemPrefersLight = window.matchMedia(
        "(prefers-color-scheme: light)"
      ).matches;
      const defaultTheme = systemPrefersLight ? "light" : "dark";
      setTheme(defaultTheme);
    }
  }, []);

  toggleTheme = () => {
    setTheme((prevTheme) => {
      const newTheme = prevTheme === "dark" ? "light" : "dark";
      localStorage.setItem("theme", newTheme);
      return newTheme;
    });
  };

  return (
    <ThemeContext.Provider value={theme}>
      <style jsx global>{`
        ${theme === "dark" ? darkTheme : lightTheme}
      `}</style>
      {children}
    </ThemeContext.Provider>
  );
};

// Boton de control para temas Dark & Light
export const ChangeTheme = ({ className }) => {
  const [theme, setTheme] = useState("dark");

  useEffect(() => {
    const savedTheme = localStorage.getItem("theme");
    if (savedTheme) {
      setTheme(savedTheme);
    } else {
      const systemPrefersLight = window.matchMedia(
        "(prefers-color-scheme: light)"
      ).matches;
      setTheme(systemPrefersLight ? "light" : "dark");
    }
  }, []);

  const handleToggleTheme = () => {
    toggleTheme();
    setTheme((prevTheme) => (prevTheme === "dark" ? "light" : "dark"));
  };

  return (
    <button className={`${s.btn_theme} ${className}`} onClick={handleToggleTheme}>
      <Image
        src={theme === "dark" ? IconDark : IconLight}
        width="17"
        height="17"
        alt="Theme Icon"
      />
    </button>
  );
};

// Boton de control para mobiles
export const ChangeThemeMobi = ({ className = s.btn_theme }) => {
  const [theme, setTheme] = useState("dark");

  useEffect(() => {
    const savedTheme = localStorage.getItem("theme");
    if (savedTheme) {
      setTheme(savedTheme);
    } else {
      const systemPrefersLight = window.matchMedia(
        "(prefers-color-scheme: light)"
      ).matches;
      setTheme(systemPrefersLight ? "light" : "dark");
    }
  }, []);
  const handleToggleTheme = () => {
    toggleTheme();
    setTheme((prevTheme) => (prevTheme === "dark" ? "light" : "dark"));
  };

  return (
    <button className={className} onClick={handleToggleTheme}>
      {theme === "dark" ? "Dark" : "Light"}

      <Image
        src={theme === "dark" ? IconDark : IconLight}
        width="17"
        height="17"
        alt="Theme Icon"
      />
    </button>
  );
};

export default ThemeProvider;

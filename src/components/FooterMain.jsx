import Image from "next/image";
import Link from "next/link";
import { langUser } from "@/scripts/langConfig.js";

// Estilos
import s from "@/css/footerMain.module.css";

// Recursos
import ProfilePic from "@/img/photo_profile.png";
import LionTeamIco from "@/img/img_foo_lionteam.png";

const FooterMain = () => {
  const allLang = langUser("home");

  return (
    <footer className={s.footer_cont}>
      <div className={s.foo_head}>
        <Image src={ProfilePic} alt="Profile" />
        <Image src={LionTeamIco} alt="LionTeam" />
      </div>
      <div className={s.foo_body}>
        <div className={s.foo_cont_links}>
          <h4>{allLang.footerSection.sections}</h4>
          {allLang.footerSection.navList.map((item) => {
            return (
              <Link href={item.link} key={item.title}>
                {item.title}
              </Link>
            );
          })}
        </div>
        <div className={s.subsec_foolinks}>
          <div className={s.foo_cont_links}>
            <h4>{allLang.footerSection.socialNetworks}</h4>
            <div className={s.foo_link_net}>
              <Link href="#">
                <Image
                  src="/icons/icon_gmail.svg"
                  width={25}
                  height={20}
                  alt="ico"
                />
              </Link>
              <Link href="https://www.linkedin.com/in/jeisson-le%C3%B3n-53b05a1a8/">
                <Image
                  src="/icons/icon_linkedin.svg"
                  width={20}
                  height={20}
                  alt="ico"
                />
              </Link>
              <Link href="#">
                <Image
                  src="/icons/icon_gitlab.svg"
                  width={20}
                  height={20}
                  alt="ico"
                />
              </Link>
              <Link href="#" className={s.special_link}>
                <Image
                  src="/icons/icon_github.svg"
                  width={20}
                  height={20}
                  alt="ico"
                />
              </Link>
              <Link href="#">
                <Image
                  src="/icons/icon_twitter.svg"
                  width={20}
                  height={20}
                  alt="ico"
                />
              </Link>
              <Link href="#">
                <Image
                  src="/icons/icon_telegram.svg"
                  width={20}
                  height={20}
                  alt="ico"
                />
              </Link>
              <Link href="#" className={s.special_link}>
                <Image
                  src="/icons/icon_instagram.svg"
                  width={20}
                  height={20}
                  alt="ico"
                />
              </Link>
            </div>
          </div>
          <div className={s.foo_cont_links}>
            <Link href="/">{allLang.footerSection.termsUse}</Link>
            <Link href="/projects">{allLang.footerSection.privacyPolicy}</Link>
          </div>
        </div>
      </div>
      <div className={s.foo_footer}>
        <span className={s.cont_foo_bottom}>
          <p>{allLang.footerSection.fooAuthor}</p>
        </span>
        <span className={s.div_foo} />
        <p className={s.tx_foo_copy}>{allLang.footerSection.fooCopyright}</p>
      </div>
    </footer>
  );
};

export default FooterMain;

"use client";

import { useState } from "react";
import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";

// Componentes
import IconArrowLeft from "@/components/icons/IconArrowLeft";
import { ChangeThemeMobi } from "./ThemeControl";
import LetsTalk from "./LetsTalk/LetsTalk";

// Estilos
import s from "@/css/headerMain.module.css";

// Exportaciones
export let ButtonLetsTalk = () => {}; //? Componente base para el despliegue del modal de contacto

const LinkNavWide = ({ title, link }) => {
  const pathname = usePathname();
  return (
    <Link
      href={link}
      className={`${s.nav_link} ${pathname === link ? s.link_active : ""}`}
    >
      {title}
    </Link>
  );
};

const SectionMobil = ({ navList, theme }) => {
  const [deployState, setDeployState] = useState(false);
  const pathname = usePathname();

  const LinkNavMobi = ({ title, link }) => {
    return (
      <Link
        href={link}
        className={`${s.nav_link_mobi} ${
          pathname === link ? s.link_active_mobi : ""
        }`}
        onClick={() => setDeployState(!deployState)}
      >
        {title}
        <IconArrowLeft
          width={6}
          height={12}
          style={{ transform: "rotate(180deg)" }}
          color="var(--color-elements)"
        />
      </Link>
    );
  };

  return (
    <section className={s.hea_sec_mobil}>
      <button
        className={s.btn_menu}
        onClick={() => setDeployState(!deployState)}
      >
        Menu
        <IconArrowLeft
          width={8}
          height={12}
          style={{ transform: `rotate(${deployState ? "-90" : "90"}deg)` }}
          color="var(--color-contrast-700-t)"
        />
      </button>

      <div
        className={s.modal_menu}
        style={{ display: deployState ? "flex" : "none" }}
      >
        <div className={s.modal_content_links}>
          {navList.map((item) => {
            return <LinkNavMobi key={item.title} {...item} />;
          })}
        </div>
        <div className={s.modal_extra_options}>
          <p className={s.modal_tx_info}>{theme}</p>
          <ChangeThemeMobi className={s.btn_option} />
        </div>
      </div>
    </section>
  );
};

const HeaderControl = ({ navList, dic }) => {
  const [TalkState, setTalkState] = useState(false);

  ButtonLetsTalk = ({ className, lang }) => {
    return (
      <div className={className} onClick={() => setTalkState(true)}>
        {lang}
      </div>
    );
  };

  return (
    <>
      <header
        className={s.hea_main}
      >
        <div className={s.hea_cont}>
          <section className={s.hea_sec_1}>
            {navList.map((item) => {
              return <LinkNavWide key={item.title} {...item} />;
            })}
          </section>

          {/* Aqui se renderiza el menu mobile y demas */}
          <SectionMobil navList={navList} theme={dic.theme} />
          <Image
            className={s.hea_img_profile}
            src="/img/pic_profile_instagram.jpg"
            width={32}
            height={32}
            alt="Profile"
          />

          <section className={s.hea_sec_2}>
            <button
              className={s.btn_talk}
              onClick={() => setTalkState(!TalkState)}
            >
              {dic.talkButton}
            </button>
          </section>
        </div>
      </header>

      {TalkState && (
        <LetsTalk
          dic={dic.floatModalWork}
          funcClose={() => setTalkState(false)}
        />
      )}
    </>
  );
};

export default HeaderControl;

//? Componente de aviso de que todavia este portafolio continua en desarrollo

"use client";
import { useState, useEffect } from "react";
import Image from "next/image";

// Estilos
import s from "./keepindev.module.css";

// Recursos
import IconX from "@/components/icons/IconX";

const KeepInDev = ({ AdviseInfo }) => {
  const { type, title, author } = AdviseInfo;

  // Estado de despliegue del Aviso
  const [modalState, setModalState] = useState(false);

  // Estado para controlar si la página está lista
  const [isPageLoaded, setIsPageLoaded] = useState(false);

  // Estado para controlar el contador
  const [counter, setCounter] = useState(6);

  // Estado para controlar si el botón de cerrar está habilitado
  const [isCloseButtonEnabled, setIsCloseButtonEnabled] = useState(false);

  useEffect(() => {
    // Verificar si es la primera visita
    const hasSeenModal = localStorage.getItem("hasSeenDevModal");

    // Si ya lo ha visto, no mostrar el modal
    if (hasSeenModal) {
      setModalState(false);
      return;
    }

    // Marcar que la página está cargada
    const handleLoad = () => {
      setIsPageLoaded(true);
    };

    // Si el documento ya está cargado
    if (document.readyState === "complete") {
      handleLoad();
    } else {
      // Si no, escuchar el evento load
      window.addEventListener("load", handleLoad);
    }

    // Limpieza del event listener
    return () => {
      window.removeEventListener("load", handleLoad);
    };
  }, []);

  useEffect(() => {
    //? Si la página está cargada, esperar 3 segundos y mostrar el modal
    if (isPageLoaded) {
      const timer = setTimeout(() => {
        setModalState(true);
      }, 3000); // 3 segundos

      return () => clearTimeout(timer);
    }
  }, [isPageLoaded]);

  useEffect(() => {
    // Si el modal está visible, iniciar el contador
    if (modalState && counter > 0) {
      const timer = setTimeout(() => {
        setCounter(counter - 1);
      }, 1000); // 1 segundo

      return () => clearTimeout(timer);
    } else if (modalState && counter === 0) {
      setIsCloseButtonEnabled(true);
    }
  }, [modalState, counter]);

  // Manejador para cerrar el modal
  const handleCloseModal = () => {
    if (isCloseButtonEnabled) {
      setModalState(false);
      // Guardar en localStorage que el usuario ya vio el modal
      localStorage.setItem("hasSeenDevModal", "true");
    }
  };

  return (
    modalState && (
      <>
        <div className={s.modal_advise}>
          <div className={s.head}>
            <button
              className={s.btn_close_float}
              onClick={handleCloseModal}
              disabled={!isCloseButtonEnabled}
              style={{ cursor: isCloseButtonEnabled ? "pointer" : "default" }}
            >
              {isCloseButtonEnabled ? (
                <IconX color="var(--cl-x)" />
              ) : (
                <span>{counter}</span>
              )}
            </button>
          </div>
          <Image
            src="/img/pic_profile_instagram.jpg"
            className={s.img_profile}
            width={32}
            height={32}
            alt="Jei"
          />
          <span className={s.tag_type}>{type} !</span>
          <p className={s.info_tx}>{title}</p>
          <p className={s.info_author}>~ {author}</p>
        </div>
        <div className={s.back_float} onClick={handleCloseModal} />
      </>
    )
  );
};

export default KeepInDev;

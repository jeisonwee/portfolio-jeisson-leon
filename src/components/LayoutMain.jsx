/* --------------------------------------------------- */
/* Estructura por defecto Layout - Componente separado */
/* --------------------------------------------------- */

// Components
import HeaderMain from "./HeaderMain";

// Styles
import s from "@/css/layoutM.module.css";

const LayoutMain = ({ children }) => {
  return (
    <div className={s.principal_cont}>
      <HeaderMain />
      <main className={s.main_cont}>{children}</main>
    </div>
  );
};

export default LayoutMain;

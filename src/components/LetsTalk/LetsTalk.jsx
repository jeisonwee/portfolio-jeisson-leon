"use client";

import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

// Estilos
import s from "./letstalk.module.css";

// Recursos
import ImgBackColors from "@/img/img_back_colors.webp";
import IconX from "@/components/icons/IconX";
import IconCommunity from "@/img/icons/icon_comunication.svg";
import BetaTag from "@/img/beta_tag.svg";

const LetsTalk = ({ dic, funcClose }) => {
  // Estado de disponibilidad
  const valCal = true;

  // Informacion de contacto
  const infoList = {
    emails: [
      {
        ico: "/icons/icon_gmail.svg",
        cont: "jeissonwee.dev@gmail.com",
      },
      {
        ico: "/icons/icon_gmail.svg",
        cont: "jei.buss@gmail.com",
      },
    ],
    redes: [
      {
        cont: "@Jeisson Leon",
        link: "https://www.linkedin.com/in/jeisson-le%C3%B3n-53b05a1a8/",
        ico: "/icons/icon_linkedin.svg",
      },
      {
        cont: "@jeissonwee.dev",
        link: "https://gitlab.com/jeissonwee.dev",
        ico: "/icons/icon_gitlab.svg",
      },
      {
        cont: "@jeisonwee",
        link: "https://gitlab.com/jeisonwee",
        ico: "/icons/icon_gitlab.svg",
      },
      {
        cont: "@JeissonWeeDev",
        link: "https://github.com/JeissonWeeDev",
        ico: "/icons/icon_github.svg",
      },
      {
        cont: "@jeissonwee",
        link: "https://www.instagram.com/jeissonwee/",
        ico: "/icons/icon_instagram.svg",
      },
    ],
  };

  const ButtonInfo = ({ icon, children }) => {
    const [isCopied, setIsCopied] = useState(false);

    const handleCopyClick = async () => {
      try {
        // Check if navigator.clipboard is supported
        if ("clipboard" in navigator) {
          await navigator.clipboard.writeText(children);
        } else {
          // Fallback for older browsers
          const textArea = document.createElement("textarea");
          textArea.value = children;
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("copy");
          document.body.removeChild(textArea);
        }
        setIsCopied(true);
        setTimeout(() => {
          setIsCopied(false);
        }, 8000); // Reset the state after 1.5 seconds
      } catch (err) {
        console.error("Unable to copy to clipboard.", err);
      }
    };

    return (
      <button className={s.btn_info} onClick={handleCopyClick}>
        <Image src={icon} width={16} height={12} alt="Ico" loading="lazy" />
        {children}

        {isCopied ? (
          <svg
            width={16}
            height={16}
            viewBox="0 0 16 16"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            style={{ marginLeft: ".5rem" }}

          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M15.724 3.276a.94.94 0 0 1 0 1.33l-8.08 8.081a2.823 2.823 0 0 1-3.993 0L.276 9.313a.941.941 0 1 1 1.33-1.331l3.376 3.374a.94.94 0 0 0 1.33 0l8.081-8.08a.94.94 0 0 1 1.331 0"
              fill="var(--color-ready)"
            />
          </svg>
        ) : (
          <svg
            width={16}
            height={16}
            style={{ marginLeft: ".5rem" }}
            viewBox="0 0 14 16"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M1.75 2.353c0-.365.296-.662.662-.662H9.47a.75.75 0 0 0 0-1.5H2.41A2.16 2.16 0 0 0 .25 2.353v8.47a.75.75 0 0 0 1.5 0zm3.485.662a2.16 2.16 0 0 0-2.161 2.162v8.47c0 1.194.967 2.162 2.161 2.162h6.353a2.16 2.16 0 0 0 2.162-2.162v-8.47a2.16 2.16 0 0 0-2.162-2.162zm-.661 2.162c0-.366.296-.662.661-.662h6.353c.366 0 .662.296.662.662v8.47a.66.66 0 0 1-.662.662H5.235a.66.66 0 0 1-.661-.662z"
              fill="var(--color-contrast-700-t)"
            />
          </svg>
        )}
      </button>
    );
  };

  const LinkInfo = ({ icon, link, children }) => {
    return (
      <Link className={s.btn_info} href={link} target="_blank" rel="noreferrer">
        <Image src={icon} width={16} height={16} alt="Ico" loading="lazy" />
        {children}
      </Link>
    );
  };

  return (
    <>
      <div className={s.modal_talk}>
        <div className={s.talk_container}>
          <section className={s.talk_head}>
            <Image
              src="/img/pic_profile_instagram.jpg"
              width={32}
              height={32}
              alt="Profile"
            />

            <button className={s.btn_close_float} onClick={() => funcClose()}>
              <IconX color="var(--cl-x)" />
            </button>
          </section>
          <section className={s.talk_sec1}>
            <Image src={IconCommunity} alt="IconColor" />
            <h2>{dic.title}</h2>
            <p>
              {dic.subTitle}
            </p>
          </section>
          <section className={s.talk_buttons}>
            <span className={s.ry_div} />

            <div className={s.state_of}>
              <p>
                <span />
                {valCal ? dic.validateCalendar.true : dic.validateCalendar.false}
              </p>
              <div className={s.tag_info_state}>
                <svg
                  width={7}
                  height={11}
                  viewBox="0 0 7 11"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M.873 6.256a1 1 0 0 1 0-1.512l4.4-3.811a1 1 0 0 1 1.655.756V9.31a1 1 0 0 1-1.654.756z"
                    fill="var(--color-back)"
                    fillOpacity={0.5}
                  />
                </svg>
                {dic.validateCalendar.guide}
              </div>
            </div>

            <div className={s.cont_btns}>
              <Link
                className={s.btn_talk}
                href="mailto:jeissonwee.dev@gmail.com?subject=Tengo una duda 💙"
              >
                {dic.btnContact}
              </Link>
              <button className={s.btn_chat}>
                {dic.btnTalk}
                <Image src={BetaTag} alt="beta" />
              </button>
            </div>
          </section>
          <section className={s.cont_info_sub_sections}>
            <div className={s.cont_section}>
              <h5>{dic.secEmails.title}:</h5>
              <div>
                {infoList.emails.map((item, index) => {
                  return (
                    <ButtonInfo key={index} icon={item.ico}>
                      {item.cont}
                    </ButtonInfo>
                  );
                })}
              </div>
            </div>
            <div className={s.cont_section}>
              <h5>{dic.secSocialNet.title}:</h5>
              <div>
                {infoList.redes.map((item, index) => {
                  return (
                    <LinkInfo key={index} icon={item.ico} link={item.link}>
                      {item.cont}
                    </LinkInfo>
                  );
                })}
              </div>
            </div>
          </section>
          <section className={s.footer_talk}>
            <p>
              {dic.footerCont}
            </p>
          </section>
        </div>

        <Image
          src={ImgBackColors}
          className={s.img_back_colors}
          alt="Colores"
        />
      </div>

      <div className={s.back_float} onClick={() => funcClose()} />
    </>
  );
};
export default LetsTalk;

"use client";

import { useRef, useState, useEffect } from "react";
import Link from "next/link";

// Estilos
import s from "./card.module.css";

let widthStandard = 0;

const CardLink = ({ link = "#", className, children, isTwoCol, isTwoRow }) => {
  const cardRef = useRef(null);
  const [height, setHeight] = useState('auto');

  useEffect(() => {
    if (cardRef.current) {
      const width = cardRef.current.getBoundingClientRect().width;

      if (!isTwoCol) {
        setHeight(width);
        widthStandard = width;
      } else if (isTwoCol && widthStandard) {
        setHeight(widthStandard);
      }

      if (isTwoRow) {
        setHeight(widthStandard + 40 + 16);
      }
    }
  }, [isTwoCol, isTwoRow]);

  return (
    <Link
      href={link}
      ref={cardRef}
      className={`${s.item} ${className}`}
      style={{ height: height }}
    >
      {children}
    </Link>
  );
};

export default CardLink;

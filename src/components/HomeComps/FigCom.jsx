'use client'

import { useState, useEffect, useMemo } from "react";

/* 
  Interpolacion de animacion segun los cuatro puntos cardinales
  0deg, -90deg, 180deg, 90deg
*/

const lerp = (start, end, t) => {
  return start * (1 - t) + end * t;
};

const DegAnimation = () => {
  const targetGradients = useMemo(() => [
    { x1: 65.921, y1: 2.496, x2: 65.197, y2: 119.849 }, // 0deg
    { x1: 6.881, y1: 61.173, x2: 124.237, y2: 61.173 }, // -90deg
    { x1: 65.559, y1: 119.85, x2: 65.559, y2: 2.495 },  // 180deg
    { x1: 124.237, y1: 61.173, x2: 6.881, y2: 61.173 },  // 90deg
  ], []);

  const [gradient, setGradient] = useState(targetGradients[0]);
  const [gradientIndex, setGradientIndex] = useState(0);
  const [t, setT] = useState(0);

  /* useEffect(() => {
    const updateGradient = () => {
      if (t < 1) {
        setT(t + 0.01);
      } else {
        setT(0);
        setGradientIndex((current) => (current + 1) % targetGradients.length);
      }
      
      const currentGradient = targetGradients[gradientIndex];
      const nextGradient =
        targetGradients[(gradientIndex + 1) % targetGradients.length];

      setGradient({
        x1: lerp(currentGradient.x1, nextGradient.x1, t),
        y1: lerp(currentGradient.y1, nextGradient.y1, t),
        x2: lerp(currentGradient.x2, nextGradient.x2, t),
        y2: lerp(currentGradient.y2, nextGradient.y2, t),
      });
    };

    const animationFrame = requestAnimationFrame(updateGradient);

    return () => cancelAnimationFrame(animationFrame);
  }, [t, gradientIndex, targetGradients]); */

  return (
    <defs>
      <linearGradient
        id="al"
        x1={gradient.x1.toString()}
        y1={gradient.y1.toString()}
        x2={gradient.x2.toString()}
        y2={gradient.y2.toString()}
        gradientUnits="userSpaceOnUse">
        <stop stopColor="var(--color-primary, yellow)" />
        <stop offset="0.5" stopColor="#fff" stopOpacity="0" />
      </linearGradient>
    </defs>
  );
};

const SVGComponent = ({ clase }) => (
  <svg
    width={144}
    height={129}
    className={clase}
    viewBox="0 0 134 119"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M33.903 12.755a7.62 7.62 0 0 0-6.73 5.92 6 6 0 0 1-.056.23L12.82 72.254a12.407 12.407 0 0 0 11.07 14.633l.19.02c6.366.795 13.082 2.207 19.083 4.608 2.605 1.042 5.167 2.308 7.538 3.852L68.098 30.44a35.84 35.84 0 0 0-34.195-17.685Zm-1.05-11.562C50.201-.372 66.829 7.688 76.411 21.856a47.45 47.45 0 0 1 48.052 3.884l.012.008a19.235 19.235 0 0 1 7.288 21.308l-14.32 53.442a6 6 0 0 1-.108.354 24 24 0 0 1-13.262 14.38 24 24 0 0 1-19.467-.212c-5.33-2.245-10.824-3.993-15.886-4.721-5.14-.74-9.332-.358-12.43 1.09a5.8 5.8 0 0 1-3.963.348 5.8 5.8 0 0 1-3.258-2.282c-1.96-2.804-5.398-5.23-10.22-7.159-4.748-1.901-10.379-3.134-16.119-3.855A24.02 24.02 0 0 1 1.47 69.806q.036-.182.084-.36l14.32-53.443a19.23 19.23 0 0 1 16.965-14.81zm46.464 32.253L61.92 98.374c2.825-.151 5.677.033 8.454.433 6.398.92 12.92 3.056 18.83 5.552l.175.077a12.407 12.407 0 0 0 16.902-7.139l14.295-53.35q.03-.115.066-.227a7.62 7.62 0 0 0-2.869-8.492 35.84 35.84 0 0 0-38.456-1.782Zm-43.604 5.727a5.805 5.805 0 1 0-3.005 11.214l14.953 4.007a5.805 5.805 0 1 0 3.005-11.215zm52.336 14.023a5.805 5.805 0 1 0-3.006 11.215l14.953 4.006a5.805 5.805 0 1 0 3.005-11.214zm-56.343.93a5.805 5.805 0 0 0-3.005 11.214l14.953 4.007a5.805 5.805 0 1 0 3.005-11.215zm52.336 14.023a5.805 5.805 0 1 0-3.005 11.215L95.99 83.37a5.805 5.805 0 0 0 3.005-11.214z"
      fill="var(--color-contrast-500)"
      opacity={0.3}
      stroke="url(#al)"
      strokeOpacity={0.59}
      strokeWidth={2.5}
    />
    <DegAnimation />
  </svg>
);

export default SVGComponent;

const SVGComponent = (props) => (
  <svg
    width={97}
    height={119}
    viewBox="0 0 97 119"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M10.727 45.964a4.054 4.054 0 0 1 2.252-5.272l35.113-14.097a4.054 4.054 0 1 1 3.021 7.524L16 48.216a4.054 4.054 0 0 1-5.272-2.252"
      fill="url(#df)"
    />
    <path
      d="M19.02 55.74a4.054 4.054 0 0 0 3.02 7.525l25.082-10.07a4.054 4.054 0 1 0-3.02-7.524z"
      fill="url(#bf)"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2.415 21.634c-13.16 5.282-19.544 20.233-14.262 33.393l20.139 50.162c4.17 10.389 15.974 15.43 26.363 11.259l55.179-22.152a9.46 9.46 0 0 0 5.254-12.302L64.88 6.75a9.46 9.46 0 0 0-12.303-5.255zM75.984 56.17 57.356 9.77a1.35 1.35 0 0 0-1.758-.751L5.437 29.158c-9.004 3.615-13.373 13.844-9.758 22.848L9.774 87.118a20.18 20.18 0 0 1 9.777-8.292zm3.02 7.524L22.573 86.351c-6.234 2.502-9.258 9.584-6.756 15.817 2.503 6.234 9.584 9.258 15.818 6.756l55.179-22.152a1.35 1.35 0 0 0 .75-1.758z"
      fill="url(#cf)"
    />
    <path
      d="M10.727 45.964a4.054 4.054 0 0 1 2.252-5.272l35.113-14.097a4.054 4.054 0 1 1 3.021 7.524L16 48.216a4.054 4.054 0 0 1-5.272-2.252Zm8.293 9.776a4.054 4.054 0 0 0 3.02 7.525l25.082-10.07a4.054 4.054 0 1 0-3.02-7.524z"
      stroke="var(--color-contrast-80-t)"
      strokeLinecap="round"
    />
    <path
      clipRule="evenodd"
      d="M2.415 21.634c-13.16 5.282-19.544 20.233-14.262 33.393l20.139 50.162c4.17 10.389 15.974 15.43 26.363 11.259l55.179-22.152a9.46 9.46 0 0 0 5.254-12.302L64.88 6.75a9.46 9.46 0 0 0-12.303-5.255zM75.984 56.17 57.356 9.77a1.35 1.35 0 0 0-1.758-.751L5.437 29.158c-9.004 3.615-13.373 13.844-9.758 22.848L9.774 87.118a20.18 20.18 0 0 1 9.777-8.292zm3.02 7.524L22.573 86.351c-6.234 2.502-9.258 9.584-6.756 15.817 2.503 6.234 9.584 9.258 15.818 6.756l55.179-22.152a1.35 1.35 0 0 0 .75-1.758z"
      stroke="var(--color-contrast-80-t)"
      strokeLinecap="round"
    />
    <defs>
      <radialGradient
        id="df"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="rotate(118.203 31.377 17.376)scale(94.7484 108.54)"
      >
        <stop stopColor="var(--color-contrast-500)" />
        <stop offset={1} stopColor="var(--color-contrast-500)" stopOpacity={0.16} />
      </radialGradient>
      <radialGradient
        id="bf"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="rotate(118.203 31.377 17.376)scale(94.7484 108.54)"
      >
        <stop stopColor="var(--color-contrast-500)" />
        <stop offset={1} stopColor="var(--color-contrast-500)" stopOpacity={0.16} />
      </radialGradient>
      <radialGradient
        id="cf"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="rotate(118.203 31.377 17.376)scale(94.7484 108.54)"
      >
        <stop stopColor="var(--color-contrast-500)" />
        <stop offset={1} stopColor="var(--color-contrast-500)" stopOpacity={0.16} />
      </radialGradient>
    </defs>
  </svg>
);
export default SVGComponent;

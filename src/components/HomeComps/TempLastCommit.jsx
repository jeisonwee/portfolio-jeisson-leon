"use client"

import { useState, useEffect } from "react";

const LatestCommitMessage = ({ apiUrl }) => {
  const [commitMessage, setCommitMessage] = useState("Loading commit...");

  useEffect(() => {
    const fetchLatestCommitMessage = async () => {
      let projectsUrl = apiUrl; //! REDUNDANTE ESTA MIERDA PRBO!!!!

      try {
        // Fetch the project(s) info
        const projectsResponse = await fetch(projectsUrl);
        const projectsData = await projectsResponse.json();
        let commitsUrl;

        if (projectsResponse.ok && projectsData.length > 0) {
          // Construct commitsUrl based on the platform detected from apiUrl
          if (apiUrl.includes("github.com")) {
            const projectName = projectsData[0].name;
            commitsUrl = `https://api.github.com/repos/JeissonWeeDev/${projectName}/commits?per_page=1`;
          } else if (apiUrl.includes("gitlab.com")) {
            const projectId = projectsData[0].id;
            commitsUrl = `https://gitlab.com/api/v4/projects/${projectId}/repository/commits?per_page=1`;
          }

          // Fetch the latest commit message
          if (commitsUrl) {
            const commitsResponse = await fetch(commitsUrl);
            const commitsData = await commitsResponse.json();
            if (commitsResponse.ok && commitsData.length > 0) {
              const latestCommitMessage = apiUrl.includes("github.com")
                ? commitsData[0].commit.message
                : commitsData[0].title;
              setCommitMessage(latestCommitMessage);
            } else {
              setCommitMessage("No commits found.");
            }
          }
        } else {
          setCommitMessage("No projects found.");
        }
      } catch (error) {
        console.error("Failed to fetch the latest commit message:", error);
        setCommitMessage("Failed to retrieve commit information.");
      }
    };

    fetchLatestCommitMessage();
  }, [apiUrl]);

  return <b>{commitMessage}</b>;
};

export default LatestCommitMessage;

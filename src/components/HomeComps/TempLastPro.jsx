"use client";

import { useState, useEffect } from "react";

const TempCont = ({ content }) => {
  const [contentG, setContentG] = useState("Probando hehe");

  useEffect(() => {
    if (content) {
      setContentG(content[0].name);
    } else {
      setContentG("No se encontraron repositorios.");
    }
  }, [content]);

  return <b>{contentG}</b>;
};

export default TempCont;

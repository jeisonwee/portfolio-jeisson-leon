"use client"

import Link from "next/link";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useTheme } from "../ThemeControl";

// Estilos
import s from "./areaSkill.module.css";

// Recursos
import IconFrontend from "@/img/icons/icon_frontend.svg";
import ImgFrontend from "@/img/img_frontend.svg";
import ImgFrontendLight from "@/img/img_frontend_light.svg";

import IconBackend from "@/img/icons/icon_backend.svg";
import ImgBackend from "@/img/img_backend.svg";
import ImgBackendLight from "@/img/img_backend_light.svg";

import IconFullstack from "@/img/icons/icon_fullstack.svg";
import ImgFullstack from "@/img/img_fullstack.svg";
import ImgFullstackLight from "@/img/img_fullstack_light.svg";

import IconUiuxdesign from "@/img/icons/icon_uiuxdesign.svg";
import ImgUiuxdesign from "@/img/img_uiuxdesign.svg";
import ImgUiuxdesignLight from "@/img/img_uiuxdesign_light.svg";

import IconArrowLeft from "@/components/icons/IconArrowLeft";

function AreaSkill({ langInfo }) {
  // Esta es basicamente una maña para evitar la re renderizacion
  const [theme, setTheme] = useState();
  const themeContext = useTheme();

  useEffect(() => {
    setTheme(themeContext === "light" ? true : false);
  }, [themeContext])

  return (
    <article className={s.page_sec_4}>
      <div className={s.sec4_sec1}>
        <div className={s.tag_container_sec4}>{langInfo.tagAreas}</div>
        <h2 className={s.sec4_title}>{langInfo.titleAreas}</h2>
        <Link href="/services" className={s.sec4_btn_details}>
          {langInfo.btnDetails}
        </Link>
        <span className={s.sec4_backcolor} />
      </div>

      <div className={s.sec4_sec2}>
        <div className={s.card_sec4}>
          <div className={s.card_sec4_content}>
            <div className={s.cont_area_ico}>
              <Image src={IconFrontend} width={24} height={24} alt="Frontend" />
            </div>
            <h3>Front-end</h3>
            <p>{langInfo.frontText}</p>
          </div>
          <Image className={s.img_area} src={theme ? ImgFrontendLight : ImgFrontend} alt="Frontend" />
        </div>
        <div className={s.card_sec4}>
          <div className={s.card_sec4_content}>
            <div className={s.cont_area_ico}>
              <Image src={IconBackend} width={24} height={24} alt="Backend" />
            </div>
            <h3>Back-end</h3>
            <p>{langInfo.backText}</p>
          </div>
          <Image className={s.img_area} src={theme ? ImgBackendLight: ImgBackend} alt="Backend" />
        </div>
        <div className={s.card_sec4}>
          <div className={s.card_sec4_content}>
            <div className={s.cont_area_ico}>
              <Image
                src={IconFullstack}
                width={24}
                height={24}
                alt="Fullstack"
              />
            </div>
            <h3>Fullstack</h3>
            <p>{langInfo.fullText}</p>
          </div>
          <Image className={s.img_area} src={theme ? ImgFullstackLight : ImgFullstack} alt="Fullstack" />
        </div>
        <div className={s.card_sec4}>
          <div className={s.card_sec4_content}>
            <div className={s.cont_area_ico}>
              <Image
                src={IconUiuxdesign}
                width={24}
                height={24}
                alt="UiUxDesign"
              />
            </div>
            <h3>UI/UX Design</h3>
            <p>
              {langInfo.uiuxText}
            </p>
          </div>
          <Image className={s.img_area} src={theme ? ImgUiuxdesignLight : ImgUiuxdesign} alt="UIUXDesign" />
        </div>
      </div>

      <div className={s.sec4_bottom}>
        <p>{langInfo.bottomTx}</p>
        <Link href="/services" className={s.sec4_btn_details}>
          {langInfo.btnServices}
          <IconArrowLeft
            width={10}
            height={12}
            style={{ transform: "rotate(180deg)" }}
            color="var(--color-contrast-700-t)"
          />
        </Link>
      </div>
    </article>
  );
}

export default AreaSkill;

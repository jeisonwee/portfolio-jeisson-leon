"use client";

import { useRef, useState, useEffect } from "react";

// Estilos
import s from "./card.module.css";

let widthStandard = 0;

const Card = ({ className, children, isTwoCol, isTwoRow }) => {
  const cardRef = useRef(null);
  const [height, setHeight] = useState('auto');

  useEffect(() => {

    if (cardRef.current) {
      const width = cardRef.current.getBoundingClientRect().width;

      if (!isTwoCol) {
        setHeight(width);
        widthStandard = width;
      } else if (isTwoCol) {
        setHeight((width / 2));
      }

      if (isTwoRow) {
        setHeight(widthStandard + 40 + 16);
      }
    }
  }, [isTwoCol, isTwoRow]);

  return (
    <div
      ref={cardRef}
      className={`${s.item} ${className}`}
      style={{ height: height }}
    >
      {children}
    </div>
  );
};

export default Card;

"use client";

import { useState } from "react";
import Image from "next/image";

// Estilos
import s from "./sliderImages.module.css";

// Recursos
import IconArrowLeft from "@/components/icons/IconArrowLeft";

const SliderImages = ({ images = [] }) => {
  const [currentStep, setCurrentStep] = useState(0);
  const totalSteps = images.length;
  const [imageErrors, setImageErrors] = useState({});

  const nextStep = () => {
    if (currentStep < totalSteps - 1) {
      setCurrentStep(currentStep + 1);
    }
  };

  const prevStep = () => {
    if (currentStep > 0) {
      setCurrentStep(currentStep - 1);
    }
  };

  const handleError = (index) => {
    setImageErrors((prevErrors) => ({ ...prevErrors, [index]: true }));
  };

  if (images.length === 0) {
    return (
      <div
        className={s.container}
        style={{
          textAlign: "center",
          color: "red",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        Photos not found bro :(.
      </div>
    );
  }

  return (
    <div className={s.container}>
      <div
        className={s.stepsContainer}
        style={{ transform: `translateX(-${currentStep * 100}%)` }}
      >
        {images.map((image, index) => (
          <div
            key={index}
            className={s.stepContent}
            style={{
              opacity: currentStep === index ? 1 : 0,
              transition: "opacity 0.3s ease",
            }}
          >
            {imageErrors[index] ? (
              <div
                style={{
                  width: "auto",
                  height: "auto",
                  margin: "auto",
                  color: "red",
                }}
              >
                Photo Error :(
              </div>
            ) : (
              <Image
                src={image}
                alt={`Imagen ${index + 1}`}
                width={500}
                height={300}
                loading={currentStep === index ? "eager" : "lazy"}
                onError={() => handleError(index)}
              />
            )}
          </div>
        ))}
      </div>

      {/* Indicadores */}
      <div className={s.circleGroup}>
        {images.map((_, index) => (
          <div
            key={index}
            className={`${s.circle} ${currentStep === index ? s.active : ""}`}
          ></div>
        ))}
      </div>

      {/* Botones de Navegación */}
      <div className={s.navigationButtons}>
        {currentStep > 0 && (
          <button onClick={prevStep} className={`${s.btnAnt} ${s.btn}`}>
            <IconArrowLeft color="var(--color-icon)" />
          </button>
        )}
        {currentStep < totalSteps - 1 && (
          <button onClick={nextStep} className={`${s.btnNext} ${s.btn}`}>
            <IconArrowLeft color="var(--color-icon)" />
          </button>
        )}
      </div>
    </div>
  );
};

export default SliderImages;

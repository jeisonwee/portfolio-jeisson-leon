import Link from "next/link";
import Image from "next/image";

// Estilos
import s from "./contNet.module.css";

const ContNet = ({ username, linkRef, subtx, logoRef, picRef }) => {
  return (
    <Link className={s.cont_net} href={linkRef} target="blank">
      <div className={s.pic_sec}>
        <Image src={picRef} width={24} height={24} alt="Profile" />
        <Image src={logoRef} width={12} height={12} alt="Com" />
      </div>
      <div className={s.pic_sec2}>
        <h3>{username}</h3>
        <p>{subtx ? subtx : ""}</p>
      </div>
    </Link>
  );
};

export default ContNet;

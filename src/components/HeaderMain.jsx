/* --------------------------------------------------- */
/*             Componente Header principal             */
/* --------------------------------------------------- */

import { langUserPre, langUser } from "@/scripts/langConfig.js";

// Componentes
import HeaderControl from "@/components/HeaderControl.jsx";
import FloatNav from "@/components/FloatNav";

const HeaderMain = () => {
  const dic = langUser("header");
  const navList = dic.navList;
  const typeLang = langUserPre();

  return (
    <>
      <HeaderControl navList={navList} dic={dic} />

      {/* Barra inferior flotante */}
      <FloatNav info={dic.floatBar} lang={typeLang} />
    </>
  );
};

export default HeaderMain;

/* --- Controlador de ejecucion media --- */

import { NextResponse } from "next/server";

export default function middleware(req) {
  const response = NextResponse.next();
  const url = req.nextUrl.clone();

  // Priorizacion de configuracion de idioma ya establecida (Se evita la ejecucion de comprobacion doble)
  if (req.cookies.has("preferredLocale")) {
    return response;
  }

  if (
    !req.cookies.has("preferredLocale") &&
    req.headers.get("accept-language")
  ) {
    const supportedLocales = ["es", "en"];
    const defaultLocale = "en";

    let preferredLocale =
      req.headers
        .get("accept-language")
        .split(",")[0]
        .split(";")[0]
        .split("-")[0] || defaultLocale;

    preferredLocale = supportedLocales.includes(preferredLocale)
      ? preferredLocale
      : defaultLocale;

    const res = NextResponse.redirect(url);

    res.headers.append(
      "Set-Cookie",
      `preferredLocale=${preferredLocale}; Path=/; HttpOnly; SameSite=Lax`
    );

    return res;
  } else if (!req.cookies.has("preferredLocale")) {
    const defaultLocale = "en"; // Ingles por defecto

    const res = NextResponse.redirect(url);

    res.headers.append(
      "Set-Cookie",
      `preferredLocale=${defaultLocale}; Path=/; HttpOnly; SameSite=Lax`
    );

    return res;
  }

  response.json({ mess: "What u triying to do bitch?!" });
}
